const {
  pagination,
  pegawaiSaldoCutiHelper,
  pegawaiSaldoCutiHelper: { getPegawaiSaldoCuti },
  pegawaiHelper,
  jenisCutiHelper,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const moment = require("moment");
const { Pegawai, Client, JenisCuti, PegawaiSaldoCuti } = require("../../model");
const readXlsxFile = require("read-excel-file/node");
const sequelize = require("../../config/db");
const { Op } = require("sequelize");

const index = async (req, res) => {
  let user = req.user.data;
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let id_client = user.id_mapping;
      let getFilter = req.query.filter;
      let pegawaiSaldoCuti = await getPegawaiSaldoCuti(
        limit,
        skip,
        null,
        getFilter,
        id_client
      );
      let model = await pegawaiSaldoCutiHelper.PegawaiSaldoCuti.count({
        where: {
          "$pegawai.client.id_client$": id_client,
        },
        include: [
          {
            model: Pegawai,
            include: [
              {
                model: Client,
                required: true,
              },
            ],
          },
        ],
      });
      if (getFilter != null) {
        let getModel = await getPegawaiSaldoCuti(
          null,
          null,
          null,
          getFilter,
          id_client
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        pegawaiSaldoCuti = await getPegawaiSaldoCuti(
          limit,
          skip,
          search,
          getFilter,
          id_client
        );
        let getModel = await getPegawaiSaldoCuti(
          null,
          null,
          search,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pegawaiSaldoCuti,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Saldo cuti",
      url: "/admin/pegawaiSaldoCuti",
      isActive: "active",
    });

    let jenisCuti = await jenisCutiHelper.JenisCuti.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let pegawai = await pegawaiHelper.Pegawai.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./modulPegawai/pegawaiSaldoCuti/index", {
      title: "Saldo cuti",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      jenisCuti: jenisCuti,
      pegawai: pegawai,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      if (response.page == "add") {
        let getJenisCuti = await JenisCuti.findOne({
          where: {
            id_jenis_cuti: response.id_jenis_cuti,
          },
        });

        let output = {};
        output.id_jenis_cuti = response.id_jenis_cuti;
        output.id_pegawai = response.id_pegawai;
        output.periode_awal = moment(
          response.periode_awal,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.periode_akhir = moment(
          response.periode_akhir,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.saldo = getJenisCuti.saldo;
        output.is_aktif = response.is_aktif;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = dateTime;
        output.time_update = dateTime;

        let insert = await pegawaiSaldoCutiHelper.createPegawaiSaldoCuti(
          output,
          {
            transaction: t,
          }
        );
        await t.commit();
        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let getJenisCuti = await JenisCuti.findOne({
          where: {
            id_jenis_cuti: response.id_jenis_cuti,
          },
        });

        let id_pegawai_saldo_cuti = response.id_pegawai_saldo_cuti;
        let output = {};
        output.id_jenis_cuti = response.id_jenis_cuti;
        output.id_pegawai = response.id_pegawai;
        output.periode_awal = moment(
          response.periode_awal,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.periode_akhir = moment(
          response.periode_akhir,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.saldo = getJenisCuti.saldo;
        output.is_aktif = response.is_aktif;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = dateTime;
        output.time_update = dateTime;

        let update = await pegawaiSaldoCutiHelper.updatePegawaiSaldoCuti(
          output,
          id_pegawai_saldo_cuti,
          {
            transaction: t,
          }
        );

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pegawai_saldo_cuti = req.params.id_pegawai_saldo_cuti;
    const getPegawaiSaldoCuti =
      await pegawaiSaldoCutiHelper.getPegawaiSaldoCutiId(id_pegawai_saldo_cuti);
    if (getPegawaiSaldoCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai jabatan",
        result: getPegawaiSaldoCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai jabatan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const id_pegawai_saldo_cuti = req.params.id_pegawai_saldo_cuti;
    const getPegawaiSaldoCuti =
      await pegawaiSaldoCutiHelper.deletePegawaiSaldoCuti(
        id_pegawai_saldo_cuti,
        {
          transaction: t,
        }
      );

    await t.commit();

    if (getPegawaiSaldoCuti) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pegawaiSaldoCuti",
        result: getPegawaiSaldoCuti,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pegawaiSaldoCuti",
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const importData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    let user = req.user.data;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushData = [];
      rows.map(async (v, i) => {
        if (i > 0) {
          pushData.push(pushImportData(v, user));
        }
      });

      Promise.all(pushData).then(async (result) => {
        let pushData = [];
        result.map((v, i) => {
          pushData.push(...v);
        });

        let importPegawaiSaldoCuti = await PegawaiSaldoCuti.bulkCreate(
          pushData,
          {
            transaction: t,
          }
        );
        await t.commit();
        if (importPegawaiSaldoCuti) {
          return res.status(200).json({
            status: 200,
            message:
              "Berhasil import " + pushData.length + " data pegawai saldo cuti",
            result: req.body,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message:
              "Gagal import " + pushData.length + " data pegawai saldo cuti",
          });
        }
      });
    });
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushImportData = async (v, user) => {
  let pushData = [];
  if (v[1] != null) {
    let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    let pegawai = await Pegawai.findOne({
      where: {
        nama_lengkap: {
          [Op.iLike]: "%" + v[1] + "%",
        },
        id_client: user.id_mapping,
      },
    });
    let jenisCuti = await JenisCuti.findOne({
      where: {
        jenis_cuti: {
          [Op.iLike]: "%" + v[2] + "%",
        },
        id_client: user.id_mapping,
      },
    });

    pushData.push({
      id_jenis_cuti: jenisCuti.id_jenis_cuti,
      id_pegawai: pegawai.id_pegawai,
      periode_awal: v[3],
      periode_akhir: v[4],
      saldo: jenisCuti.saldo,
      is_aktif: 1,
      user_create: user.id,
      user_update: user.id,
      time_create: dateTime,
      time_update: dateTime,
    });
  }
  return pushData;
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  importData,
};
