const {
  pagination,
  pegawaiJadwalHelper,
  pegawaiJadwalHelper: { getPegawaiJadwal },
  jadwalHelper,
  pegawaiHelper,
  jenisAbsensiHelper,
  unitHelper,
} = require("../../helper/index");
const {
  Pegawai,
  PegawaiUnit,
  Client,
  Jadwal,
  PegawaiJadwal,
} = require("../../model");
const { validationResult } = require("express-validator");
var moment = require("moment");
const readXlsxFile = require("read-excel-file/node");
const { convertBulanToNumber } = require("../../utils");
const { Op } = require("sequelize");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    let user = req.user.data;

    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = req.query.filter;
      let id_client = user.id_mapping;

      let pegawaiJadwal = await getPegawaiJadwal(
        limit,
        skip,
        null,
        getFilter,
        id_client
      );

      let model = await pegawaiJadwalHelper.PegawaiJadwal.count({
        where: {
          "$pegawai.client.id_client$": user.id_mapping,
        },
        include: [
          {
            model: Pegawai,
            include: [
              {
                model: Client,
                required: true,
              },
            ],
          },
        ],
      });
      if (getFilter != null) {
        let getModel = await getPegawaiJadwal(
          null,
          null,
          null,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      if (search != null && search != "") {
        pegawaiJadwal = await getPegawaiJadwal(
          limit,
          skip,
          search,
          getFilter,
          id_client
        );

        let getModel = await getPegawaiJadwal(
          null,
          null,
          search,
          getFilter,
          id_client
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pegawaiJadwal,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Jadwal pegawai",
      url: "/admin/pegawaiJadwal",
      isActive: "active",
    });

    // passing data
    let client_id = req.query.client_id;
    if (user.jenis_mapping == "client") {
      client_id = user.id_mapping;
    }

    let jadwal = await jadwalHelper.Jadwal.findAll({
      where: {
        id_client: client_id,
      },
    });
    let pegawai = await pegawaiHelper.Pegawai.findAll({
      where: {
        id_client: client_id,
      },
    });
    let jenisAbsensi = await jenisAbsensiHelper.JenisAbsensi.findAll({
      where: {
        id_client: client_id,
      },
    });
    let unit = await unitHelper.Unit.findAll({
      where: {
        id_client: client_id,
      },
      include: [
        {
          model: PegawaiUnit,
          where: {
            is_aktif: 1,
          },
          required: true,
        },
      ],
      order: [["id_unit", "asc"]],
    });

    let currentDate = moment().format("DD-MM-YYYY");

    res.render("./modulPegawai/pegawaiJadwal/index", {
      title: "Jadwal pegawai",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      jadwal: jadwal,
      pegawai: pegawai,
      jenisAbsensi: jenisAbsensi,
      client_id: client_id,
      unit: unit,
      currentDate: currentDate,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        // total jam kerja
        let jadwal = await jadwalHelper.jadwalById(response.id_jadwal);
        let waktu_masuk = moment.duration(jadwal.waktu_masuk).asSeconds();
        let waktu_keluar = moment.duration(jadwal.waktu_keluar).asSeconds();
        let total_jam_kerja = waktu_keluar - waktu_masuk;
        total_jam_kerja = moment
          .utc(moment.duration(total_jam_kerja, "seconds").asMilliseconds())
          .format("HH:mm:ss");

        let hari_libur = response.hari_libur.split(",");
        let bulan = response.bulan.split(",");
        const startYear = moment().startOf("year").format("YYYY");

        let getData = [];
        bulan.map((v, i) => {
          getData.push(
            pushArray(v, startYear, total_jam_kerja, hari_libur, response)
          );
        });

        await Promise.all(getData).then(async (result) => {
          let resultDb = result[0];

          if (resultDb.length == 0) {
            return res.status(200).json({
              status: 200,
              message: "Data pegawai jadwal sudah ada",
            });
          }

          let insert = await pegawaiJadwalHelper.insertBatch(resultDb, {
            transaction: t,
          });
          await t.commit();

          if (insert) {
            return res.status(200).json({
              status: 200,
              message: "Berhasil insert data",
              return: response,
            });
          } else {
            return res.status(400).json({
              status: 400,
              message: "Gagal insert data",
            });
          }
        });
      } else {
        let id_pegawai_jadwal = response.id_pegawai_jadwal;
        let is_kerja = 1;

        // total jam kerja
        let jadwal = await jadwalHelper.jadwalById(response.id_jadwal);
        let waktu_masuk = moment.duration(jadwal.waktu_masuk).asSeconds();
        let waktu_keluar = moment.duration(jadwal.waktu_keluar).asSeconds();
        let total_jam_kerja = waktu_keluar - waktu_masuk;
        total_jam_kerja = moment
          .utc(moment.duration(total_jam_kerja, "seconds").asMilliseconds())
          .format("HH:mm:ss");

        let update = await pegawaiJadwalHelper.PegawaiJadwal.update(
          {
            id_pegawai: response.id_pegawai,
            id_jadwal: response.id_jadwal,
            tanggal: moment(response.tanggal, "DD-MM-YYYY").format(
              "YYYY-MM-DD"
            ),
            is_kerja: is_kerja,
            total_jam_kerja: total_jam_kerja,
          },
          {
            where: {
              id_pegawai_jadwal: id_pegawai_jadwal,
            },
            transaction: t,
          }
        );

        await t.commit();

        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushArray = async (
  value_bulan,
  startYear,
  total_jam_kerja,
  hari_libur,
  response
) => {
  let pushDb = [];
  let tanggalWaktu = startYear + "-" + value_bulan;
  // get range
  let firstDate = moment(tanggalWaktu).startOf("month").format("DD");
  let endDate = moment(tanggalWaktu).endOf("month").format("DD");

  let tanggalAwal = [startYear, value_bulan, firstDate];
  tanggalAwal = tanggalAwal.join("-");
  let tanggalAkhir = [startYear, value_bulan, endDate];
  tanggalAkhir = tanggalAkhir.join("-");

  for (i = parseInt(firstDate); i <= endDate; i++) {
    let date = i;
    if (i < 10) {
      date = "0" + i;
    }
    let tanggal = [startYear, value_bulan, date];
    tanggal = tanggal.join("-");
    let day = moment(tanggal).format("dddd");
    day = day.toLowerCase();
    let boolean = hari_libur.includes(day);
    let is_aktif = 0;
    let id_jadwal = null;
    let db_total_jam_kerja = null;
    if (!boolean) {
      is_aktif = 1;
      id_jadwal = response.id_jadwal;
      db_total_jam_kerja = total_jam_kerja;
    }
    let id_pegawai = response.id_pegawai;

    let count = await pegawaiJadwalHelper.PegawaiJadwal.count({
      where: {
        id_pegawai: id_pegawai,
        tanggal: tanggal,
      },
    });

    if (count == 0) {
      pushDb.push({
        id_pegawai: id_pegawai,
        id_jadwal: id_jadwal,
        tanggal: tanggal,
        is_kerja: is_aktif,
        total_jam_kerja: db_total_jam_kerja,
      });
    }
  }
  return pushDb;
};

const submitPerUnit = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      const unit = response.id_unit;
      const getUnit = await unitHelper.Unit.findOne({
        where: {
          id_unit: unit,
          "$unit.is_aktif$": 1,
        },
        include: [
          {
            model: PegawaiUnit,
          },
        ],
      });
      const pegawai = getUnit.pegawai_units;
      const bulan = response.bulan.split(",");
      let hari_libur = response.hari_libur.split(",");

      // total jam kerja
      let jadwal = await jadwalHelper.jadwalById(response.id_jadwal);
      let waktu_masuk = moment.duration(jadwal.waktu_masuk).asSeconds();
      let waktu_keluar = moment.duration(jadwal.waktu_keluar).asSeconds();
      let total_jam_kerja = waktu_keluar - waktu_masuk;
      total_jam_kerja = moment
        .utc(moment.duration(total_jam_kerja, "seconds").asMilliseconds())
        .format("HH:mm:ss");

      const startYear = moment().startOf("year").format("YYYY");
      let getData = [];
      bulan.map((v, i) => {
        let tanggalWaktu = startYear + "-" + v;
        // get range
        let firstDate = moment(tanggalWaktu).startOf("month").format("DD");
        let endDate = moment(tanggalWaktu).endOf("month").format("DD");

        let tanggalAwal = [startYear, v, firstDate];
        tanggalAwal = tanggalAwal.join("-");
        let tanggalAkhir = [startYear, v, endDate];
        tanggalAkhir = tanggalAkhir.join("-");

        for (i = parseInt(firstDate); i <= endDate; i++) {
          let date = i;
          if (i < 10) {
            date = "0" + i;
          }
          let tanggal = [startYear, v, date];
          tanggal = tanggal.join("-");
          let day = moment(tanggal).format("dddd");
          day = day.toLowerCase();
          let boolean = hari_libur.includes(day);
          let is_aktif = 0;
          let id_jadwal = null;
          let db_total_jam_kerja = null;
          if (!boolean) {
            is_aktif = 1;
            id_jadwal = response.id_jadwal;
            db_total_jam_kerja = total_jam_kerja;
          }
          pegawai.map((v_pegawai, i_pegawai) => {
            let push = {
              id_pegawai: v_pegawai.id_pegawai,
              id_jadwal: id_jadwal,
              tanggal: tanggal,
              is_kerja: is_aktif,
              total_jam_kerja: db_total_jam_kerja,
            };
            getData.push(pushArrByUnit(push));
          });
        }
      });

      Promise.all(getData).then(async (result) => {
        let pushDb = [];
        result.map((v, i) => {
          if (v[0] != null) {
            pushDb.push(v[0]);
          }
        });

        if (pushDb.length == 0) {
          return res.status(200).json({
            status: 200,
            message: "Data jadwal pegawai sudah ada",
          });
        }

        // end get range
        let insert = await pegawaiJadwalHelper.insertBatch(pushDb, {
          transaction: t,
        });
        await t.commit();
        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushArrByUnit = async (dataFilter = {}) => {
  let push = [];
  let countCheck = await pegawaiJadwalHelper.PegawaiJadwal.count({
    where: {
      id_pegawai: dataFilter.id_pegawai,
      tanggal: dataFilter.tanggal,
    },
  });
  if (countCheck == 0) {
    push.push(dataFilter);
  }
  return push;
};

const edit = async (req, res) => {
  try {
    const id_pegawai_jadwal = req.params.id_pegawai_jadwal;
    const getPegawaiJadwal = await pegawaiJadwalHelper.PegawaiJadwal.findOne({
      where: { id_pegawai_jadwal: id_pegawai_jadwal },
      include: {
        model: Pegawai,
      },
    });

    if (getPegawaiJadwal) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data jadwal pegawai",
        result: getPegawaiJadwal,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data jadwal pegawai",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const id_pegawai_jadwal = req.params.id_pegawai_jadwal;
    const getPegawaiJadwal = await pegawaiJadwalHelper.PegawaiJadwal.destroy({
      where: {
        id_pegawai_jadwal: id_pegawai_jadwal,
      },
      transaction: t,
    });

    await t.commit();
    if (getPegawaiJadwal) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pegawaiJadwal",
        result: getPegawaiJadwal,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pegawaiJadwal",
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pegawaiLoadData = async (req, res) => {
  try {
    const getPegawai = await pegawaiHelper.Pegawai.findAll();

    if (getPegawai) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pegawai",
        result: getPegawai,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pegawai",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const changeJadwal = async (req, res) => {
  try {
    const { id_pegawai_jadwal, id_pegawai_pengganti } = req.body;

    let update = await pegawaiJadwalHelper.PegawaiJadwal.update(
      {
        is_ganti_jadwal: 1,
        id_pegawai_pengganti: id_pegawai_pengganti,
      },
      {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
      }
    );

    if (update) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil ganti jadwal",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal ganti jadwal",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const batalGantiShift = async (req, res) => {
  try {
    const { id_pegawai_jadwal } = req.body;

    let update = await pegawaiJadwalHelper.PegawaiJadwal.update(
      {
        is_ganti_jadwal: null,
        id_pegawai_pengganti: null,
      },
      {
        where: {
          id_pegawai_jadwal: id_pegawai_jadwal,
        },
      }
    );

    if (update) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil batalkan jadwal",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal batalkan jadwal",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};
const importData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    let user = req.user.data;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let bulan = rows[0][2];
      let splitBulan = bulan.split(" ");
      let getBulan = convertBulanToNumber(splitBulan[1]);
      let getTahun = splitBulan[2];

      let tanggalWaktu = getTahun + "-" + getBulan;
      let firstDate = moment(tanggalWaktu).startOf("month").format("DD");
      let endDate = moment(tanggalWaktu).endOf("month").format("DD");

      let pushData = [];
      rows.map(async (v, i) => {
        if (i > 2) {
          pushData.push(
            pushImportData(firstDate, endDate, tanggalWaktu, v, user)
          );
        }
      });

      Promise.all(pushData).then(async (result) => {
        let pushData = [];
        result.map((v, i) => {
          pushData.push(...v);
        });

        let importJadwalPegawai = await PegawaiJadwal.bulkCreate(pushData, {
          transaction: t,
        });
        await t.commit();

        if (importJadwalPegawai) {
          return res.status(200).json({
            status: 200,
            message:
              "Berhasil import " + pushData.length + " data jadwal pegawai",
            result: req.body,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal import " + pushData.length + " data jadwal pegawai",
          });
        }
      });
    });
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushImportData = async (firstDate, endDate, tanggalWaktu, v, user) => {
  let pushData = [];
  let getPegawai = await Pegawai.findOne({
    where: {
      nama_lengkap: v[1],
      id_client: user.id_mapping,
    },
  });

  let number = 2;
  for (let i = parseInt(firstDate); i <= parseInt(endDate); i++) {
    let jadwalShift = v[number];
    let varJenis = jadwalShift;
    let jenisDb = null;
    switch (varJenis) {
      case "P":
        jenisDb = "pagi";
        break;
      case "S":
        jenisDb = "siang";
        break;
      case "M":
        jenisDb = "malam";
        break;
      case "PC":
        jenisDb = "pic";
        break;
      case "L":
        jenisDb = "libur";
        break;
      case "C":
        jenisDb = "cuti";
        break;
      case "O":
        jenisDb = "office";
        break;
    }

    let getJadwal = await Jadwal.findOne({
      where: {
        jenis: {
          [Op.iLike]: "%" + jenisDb + "%",
        },
        id_client: user.id_mapping,
      },
    });

    let total_jam_kerja_var = null;
    if (getJadwal != null) {
      let waktu_masuk_var = moment.duration(getJadwal.waktu_masuk).asSeconds();
      let waktu_keluar_var = moment
        .duration(getJadwal.waktu_keluar)
        .asSeconds();
      total_jam_kerja_var = waktu_keluar_var - waktu_masuk_var;
      total_jam_kerja_var = moment
        .utc(moment.duration(total_jam_kerja_var, "seconds").asMilliseconds())
        .format("HH:mm:ss");
    }

    let tanggalVar = i;
    if (tanggalVar < 10) {
      tanggalVar = "0" + tanggalVar;
    }
    let mergetTanggalVar = tanggalWaktu + "-" + tanggalVar;
    let getIdjadwal = getJadwal != null ? getJadwal.id_jadwal : null;

    let id_pegawai = getPegawai.id_pegawai;
    let id_jadwal = getIdjadwal;
    let tanggal = mergetTanggalVar;
    let is_kerja = jenisDb != "cuti" && jenisDb != "libur" ? 1 : 0;
    let is_hadir = null;
    let waktu_masuk = null;
    let waktu_pulang = null;
    let is_terlambat = null;
    let is_pulang_cepat = null;
    let is_cuti = jenisDb == "cuti" ? 1 : 0;
    let is_lembur = null;
    let total_jam_kerja = total_jam_kerja_var;
    let total_jam_kerja_real = null;
    let total_jam_terlambat = null;
    let total_jam_pulang_cepat = null;
    let total_jam_lembur = null;
    let alasan_terlambat = null;
    let id_jenis_absensi = null;
    let is_ganti_jadwal = null;
    let id_pegawai_pengganti = null;
    // push data array
    pushData.push({
      id_pegawai: id_pegawai,
      id_jadwal: id_jadwal,
      tanggal: tanggal,
      is_kerja: is_kerja,
      is_hadir: is_hadir,
      waktu_masuk: waktu_masuk,
      waktu_pulang: waktu_pulang,
      is_terlambat: is_terlambat,
      is_pulang_cepat: is_pulang_cepat,
      is_cuti: is_cuti,
      is_lembur: is_lembur,
      total_jam_kerja: total_jam_kerja,
      total_jam_kerja_real: total_jam_kerja_real,
      total_jam_terlambat: total_jam_terlambat,
      total_jam_pulang_cepat: total_jam_pulang_cepat,
      total_jam_lembur: total_jam_lembur,
      alasan_terlambat: alasan_terlambat,
      id_jenis_absensi: id_jenis_absensi,
      is_ganti_jadwal: is_ganti_jadwal,
      id_pegawai_pengganti,
    });

    number++;
  }
  return pushData;
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  pegawaiLoadData,
  changeJadwal,
  batalGantiShift,
  submitPerUnit,
  importData,
};
