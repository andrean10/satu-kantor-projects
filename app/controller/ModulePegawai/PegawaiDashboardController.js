const { getPegawaiJadwal } = require("../../helper/pegawaiJadwalHelper");

const index = async (req, res) => {
  try {
    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });

    breadcrumb.push({
      label: "Pegawai Dashboard",
      url: "/admin/pegawaiDashboard",
      isActive: "active",
    });

    let client_id = req.query.client_id;
    res.render("./modulPegawai/dashboard/index", {
      title: "Dashboard Pegawai",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      client_id: client_id,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
};
