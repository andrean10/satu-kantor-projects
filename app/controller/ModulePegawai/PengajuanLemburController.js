const moment = require("moment");
const {
  Pegawai,
  PengajuanLembur,
  PengajuanLemburList,
  Client,
  JenisLembur,
  Cabang,
  PegawaiKontrak,
} = require("../../model");
const {
  pengajuanLemburHelper,
  pengajuanLemburHelper: { getPengajuanLembur, getPengajuanLemburId },
  pagination,
} = require("../../helper/index");
const { validationResult } = require("express-validator");
const mv = require("mv");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  let user = req.user.data;
  try {
    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halmaanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halmaanAwal;

      let getFilter = req.query.filter;
      let pengajuanLembur = await getPengajuanLembur(
        limit,
        skip,
        null,
        getFilter,
        user.id_mapping
      );
      let model = await pengajuanLemburHelper.PengajuanLembur.count({
        where: {
          id_client: user.id_mapping,
        },
      });

      if (getFilter != null) {
        let getModel = await getPengajuanLembur(
          null,
          null,
          null,
          getFilter,
          user.id_mapping
        );
        model = getModel.length;
      }
      if (search != null && search != "") {
        pengajuanLembur = await getPengajuanLembur(
          limit,
          skip,
          search,
          getFilter,
          user.id_mapping
        );
        let getModel = await getPengajuanLembur(
          null,
          null,
          search,
          getFilter,
          user.id_mapping
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pengajuanLembur,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
    breadcrumb.push({
      label: "Pengajuan lembur",
      url: "/pegawai/pengajuanLembur",
      isActive: "active",
    });

    // let pegawai = await Pegawai.findAll({
    //   where: {
    //     id_client: user.id_mapping,
    //   },
    // });
    let jenisLembur = await JenisLembur.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });
    let cabang = await Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./modulPegawai/pengajuanLembur/index", {
      title: "Pengajuan lembur",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      // pegawai: pegawai,
      jenisLembur: jenisLembur,
      cabang: cabang,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

      if (response.page == "add") {
        let berkas_db = await uploadGambar(response.berkas);

        let output = {};
        output.id_client = user.id_mapping;
        output.id_cabang = response.id_cabang;
        output.id_jenis_lembur = response.id_jenis_lembur;
        output.tanggal_lembur = moment(
          response.tanggal_lembur,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.waktu_mulai = response.waktu_mulai;
        output.waktu_selesai = response.waktu_selesai;
        output.catatan = response.catatan;
        output.berkas = berkas_db;
        output.is_delete = null;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = dateTime;
        output.time_update = dateTime;

        let insertPengajuanLembur =
          await pengajuanLemburHelper.createPengajuanLembur(output, {
            transaction: t,
          });

        // pengajuan lembur list
        let getPegawai = response.id_pegawai;
        getPegawai = getPegawai.split(",");
        let pushPengajuanLemburList = [];
        getPegawai.map((v, i) => {
          pushPengajuanLemburList.push({
            id_pengajuan_lembur: insertPengajuanLembur.id_pengajuan_lembur,
            id_pegawai: v,
            user_create: user.id_mapping,
            user_update: user.id_mapping,
            time_create: dateTime,
            time_update: dateTime,
          });
        });

        let insertPengajuanLemburList = await PengajuanLemburList.bulkCreate(
          pushPengajuanLemburList,
          {
            transaction: t,
          }
        );

        await t.commit();

        if (insertPengajuanLembur || insertPengajuanLemburList) {
          return res.json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let id_pengajuan_lembur = response.id_pengajuan_lembur;
        let berkas_db = await uploadGambar(
          response.berkas,
          id_pengajuan_lembur
        );

        let output = {};
        output.id_client = user.id_mapping;
        output.id_cabang = response.id_cabang;
        output.id_jenis_lembur = response.id_jenis_lembur;
        output.tanggal_lembur = moment(
          response.tanggal_lembur,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        output.waktu_mulai = response.waktu_mulai;
        output.waktu_selesai = response.waktu_selesai;
        output.catatan = response.catatan;
        output.berkas = berkas_db;
        output.is_delete = null;
        output.user_create = user.id;
        output.user_update = user.id;
        output.time_create = dateTime;
        output.time_update = dateTime;

        let updatePengajuanLembur =
          await pengajuanLemburHelper.updatePengajuanLembur(
            output,
            id_pengajuan_lembur,
            {
              transaction: t,
            }
          );

        let checkPengajuanLemburList = await PengajuanLemburList.count({
          where: {
            id_pengajuan_lembur: id_pengajuan_lembur,
          },
        });
        if (checkPengajuanLemburList > 0) {
          await PengajuanLemburList.destroy({
            where: {
              id_pengajuan_lembur: id_pengajuan_lembur,
            },
          });
        }

        // pengajuan lembur list
        let getPegawai = response.id_pegawai;
        getPegawai = getPegawai.split(",");
        let pushPengajuanLemburList = [];
        getPegawai.map((v, i) => {
          pushPengajuanLemburList.push({
            id_pengajuan_lembur: id_pengajuan_lembur,
            id_pegawai: v,
            user_create: user.id_mapping,
            user_update: user.id_mapping,
            time_create: dateTime,
            time_update: dateTime,
          });
        });

        let updatePengajuanLemburList = await PengajuanLemburList.bulkCreate(
          pushPengajuanLemburList,
          {
            transaction: t,
          }
        );

        await t.commit();

        if (updatePengajuanLembur || updatePengajuanLemburList) {
          return res.json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      }
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pengajuan_lembur = req.params.id_pengajuan_lembur;
    const getPengajuanLembur = await pengajuanLemburHelper.getPengajuanLemburId(
      id_pengajuan_lembur
    );

    if (getPengajuanLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pengajuan lembur",
        result: getPengajuanLembur,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pengajuan lembur",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const id_pengajuan_lembur = req.params.id_pengajuan_lembur;
    await deleteGambar(id_pengajuan_lembur);
    const getPengajuanLembur =
      await pengajuanLemburHelper.deletePengajuanLembur(id_pengajuan_lembur, {
        transaction: t,
      });

    await t.commit();
    if (getPengajuanLembur) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pengajuanLembur",
        result: getPengajuanLembur,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pengajuanLembur",
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getPegawaiCabang = async (req, res) => {
  try {
    const { id_cabang } = req.query;
    const getPegawai = await PegawaiKontrak.findAll({
      where: {
        id_cabang: id_cabang,
        is_aktif: 1,
      },
      include: [
        {
          model: Pegawai,
        },
      ],
    });

    if (getPegawai) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pengajuanLembur",
        result: getPegawai,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pengajuanLembur",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadGambar = async (responseBerkas, id_pengajuan_lembur = null) => {
  if (responseBerkas.berkas != null) {
    const { berkas } = responseBerkas;

    let pathOld = berkas.path;
    let name = berkas.name;
    let size = berkas.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/pengajuanLembur/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_pengajuan_lembur != null) {
          await deleteGambar(id_pengajuan_lembur);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_pengajuan_lembur != null) {
    let getPengajuanLembur = await getPengajuanLemburId(id_pengajuan_lembur);
    if (
      getPengajuanLembur.berkas != "default.png" &&
      getPengajuanLembur.berkas != null
    ) {
      return getPengajuanLembur.berkas;
    }
  }
  return "default.png";
};

const deleteGambar = async (id_pengajuan_lembur = null) => {
  try {
    if (id_pengajuan_lembur != null) {
      let getPengajuanLembur = await getPengajuanLemburId(id_pengajuan_lembur);
      if (
        getPengajuanLembur.berkas != "default.png" &&
        getPengajuanLembur.berkas != null
      ) {
        let unlink =
          "public/image/pengajuanLembur/" + getPengajuanLembur.berkas;
        if (fs.existsSync(unlink)) {
          fs.unlinkSync(unlink);
        }
      }
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  getPegawaiCabang,
};
