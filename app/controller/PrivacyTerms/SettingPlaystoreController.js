const index = async (req, res) => {
  try {
    res.render("./playstore/privacyPolicy", {
      layout: false,
      title: "Privacy & Policy",
    });
  } catch (err) {
    console.log(err);
  }
};
const terms = async (req, res) => {
  try {
    res.render("./playstore/terms", {
      layout: false,
      title: "Terms Condition",
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  index,
  terms,
};
