const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const moment = require("moment");
const { validationResult } = require("express-validator");

const { main } = require("../../config/gmail");
const { randomOtpNumber } = require("../../utils/index");
const {
  userMappingHelper,
  userHelper: { getUsersById, getUserEmail, Users },
} = require("../../helper");
const sequelize = require("../../config/db");

require("dotenv").config();

const index = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { email, password } = req.body;
    const checkEmail = await getUsersById(null, null, null, email);
    if (checkEmail != null) {
      let hash = checkEmail.password_hash;
      let check = bcrypt.compareSync(password, hash);

      if (check) {
        let random = randomOtpNumber();
        let id_user = checkEmail.id;
        let durasi = 2;
        let tanggalPlusTwoMinute = moment()
          .add(durasi, "minutes")
          .format("YYYY-MM-DD HH:mm:ss");

        let output = {};
        output.random = random;
        output.email = email;
        output.estimasi = tanggalPlusTwoMinute;
        output.durasi = durasi;

        // let response = await main(output);
        let response = true;
        if (response) {
          let data = {};
          data.pin = output.random;
          data.updated_at = output.estimasi;

          await Users.update(data, {
            where: {
              id: id_user,
            },
            transaction: t,
          });
          await t.commit();
          return res.status(200).json({
            status: 200,
            message: "Kode OTP terkirim ke email anda",
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal login OTP",
          });
        }
      } else {
        res.status(400).json({
          status: 400,
          message: "Password anda salah",
        });
      }
    } else {
      res.status(400).json({
        status: 400,
        message: "Username tidak ditemukan",
      });
    }
  } catch (error) {
    await t.rollback();
    res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const verify = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { email, pin } = req.body;

    const checkData = await getUserEmail(email, pin);

    if (checkData != null) {
      let dateNow = moment();
      let dateTo = moment(checkData.updated_at);
      let diff = moment.duration(dateTo.diff(dateNow)).asDays();
      if (diff > 0) {
        let jenis_mapping = checkData.user_mapping.jenis_mapping;
        let id_mapping = checkData.user_mapping.id_mapping;
        let id_user_mapping = checkData.user_mapping.id_user_mapping;

        let data = {};
        data.id = checkData.id;
        data.jenis_mapping = jenis_mapping;
        data.id_mapping = id_mapping;
        data.id_user_mapping = id_user_mapping;

        // json web token
        const token = jwt.sign(
          {
            data: data,
          },
          process.env.JWT_SECRET,
          { expiresIn: "7d" }
        );

        const getMapping = await userMappingHelper.getJoinDataMapping(
          data.jenis_mapping,
          null,
          data.id_user_mapping
        );

        return res.status(200).json({
          status: 200,
          message: "Berhasil verifikasi kode otp",
          result: getMapping,
          token: token,
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Waktu verifikasi OTP sudah berakhir",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Kesalahan OTP",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const sendOtpRedo = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }

    const { email } = req.body;

    const checkData = await getUserEmail(email);
    let id_user = checkData.id;

    if (checkData != null) {
      let random = randomOtpNumber();

      let durasi = 2;
      let tanggalPlusTwoMinute = moment()
        .add(durasi, "minutes")
        .format("YYYY-MM-DD HH:mm:ss");

      let output = {};
      output.random = random;
      output.email = email;
      output.estimasi = tanggalPlusTwoMinute;
      output.durasi = durasi;

      // let response = await main(output);
      let response = true;
      if (response) {
        let data = {};
        data.pin = output.random;
        data.updated_at = output.estimasi;

        Users.update(data, {
          where: {
            id: id_user,
          },
          transaction: t,
        });

        await t.commit();
        return res.status(200).json({
          status: 200,
          message: "Kode OTP terkirim ke email anda",
        });
      } else {
        return res.status(400).json({
          status: 400,
          message: "Gagal login OTP",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Email tidak ditemukan",
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  verify,
  sendOtpRedo,
};
