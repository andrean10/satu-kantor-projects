const {
  userMappingHelper,
  pegawaiHelper,
  unitPegawaiHelper,
} = require("../../helper/index");
const { getCabangById, Cabang } = require("../../helper/cabangHelper");
const { getPegawaiByIdJadwal } = require("../../helper/pegawaiJadwalHelper");
const moment = require("moment");

const index = async (req, res) => {
  try {
    let user = req.user.data;

    // get data maping
    const getMapping = await userMappingHelper.getJoinDataMapping(
      user.jenis_mapping,
      null,
      user.id_user_mapping
    );

    const getPegawaiUnit = await unitPegawaiHelper.PegawaiUnit.findOne({
      where: {
        id_pegawai: user.id_mapping,
        is_aktif: 1,
      },
      include: [
        {
          model: Cabang,
        },
      ],
    });

    let output = {};
    let tanggalSekarang = moment().format("YYYY-MM-DD");
    output.user = getMapping;
    output.cabang = getPegawaiUnit.cabang;
    output.jadwal = await getPegawaiByIdJadwal(
      getMapping.pegawai.id_pegawai,
      tanggalSekarang
    );

    let dataJadwal = output.jadwal;
    if (dataJadwal != null) {
      if (dataJadwal.is_ganti_jadwal == 1) {
        let id_pegawai_pengganti = dataJadwal.id_pegawai_pengganti;
        let getPegawaiPengganti = await pegawaiHelper.getPegawaiById(
          id_pegawai_pengganti
        );

        let jadwalString = JSON.stringify(output.jadwal);
        let parseJadwal = JSON.parse(jadwalString);
        let pushOutput = {
          ...output,
          ...parseJadwal,
          pegawaiPengganti: getPegawaiPengganti,
        };
        output = pushOutput;
      }
    }

    // get data jadwal
    return res.status(200).json({
      status: 200,
      message: "Berhasil mengambil data profile",
      result: output,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Gagal mengambil data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
};
