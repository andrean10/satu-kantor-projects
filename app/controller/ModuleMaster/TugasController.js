const { validationResult } = require("express-validator");
const { tugasHelper, pagination } = require("../../helper/index");
const moment = require("moment");
const { Cabang, Tugas } = require("../../model");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    let user = req.user.data;

    if (req.xhr) {
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let getFilter = {
        id_client: user.id_mapping,
      };
      getFilter = { ...getFilter, ...req.query.filter };
      let tugas = await tugasHelper.getTugas(limit, skip, null, getFilter);
      let model = await tugasHelper.Tugas.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await tugasHelper.getTugas(null, null, null, getFilter);
        model = getModel.length;
      }

      if (search != null && search != "") {
        tugas = await tugasHelper.getTugas(limit, skip, search, getFilter);
        let getModel = await tugasHelper.getTugas(
          null,
          null,
          search,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: tugas,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }

    id_client = user.id_mapping;

    // breadcrumb
    let breadcrumb = [];
    breadcrumb.push({
      label: "Dashboard",
      url: "/admin/dashboard",
    });
    breadcrumb.push({
      label: "Tugas",
      url: "/admin/tugas",
      isActive: "active",
    });

    let cabang = await Cabang.findAll({
      where: {
        id_client: user.id_mapping,
      },
    });

    res.render("./moduleMaster/tugas/index", {
      title: "Tugas",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      cabang: cabang,
    });
  } catch (err) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: err.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;
      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let data = {
          id_client: user.id_mapping,
          id_cabang: response.id_cabang,
          nama_tugas: response.nama_tugas,
          is_aktif: response.is_aktif,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
        };
        let insert = await tugasHelper.createTugas(data, {
          transaction: t,
        });
        await t.commit();

        if (insert) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        let id_tugas = response.id_tugas;
        let data = {
          id_client: user.id_mapping,
          id_cabang: response.id_cabang,
          nama_tugas: response.nama_tugas,
          is_aktif: response.is_aktif,
          user_update: user.id,
          time_update: dateTime,
        };
        let update = await tugasHelper.updateTugas(data, id_tugas, {
          transaction: t,
        });

        await t.commit();
        if (update) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi insert data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_tugas = req.params.id_tugas;
    const getTugas = await tugasHelper.getTugasId(id_tugas);
    if (getTugas) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data tugas",
        result: getTugas,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data tugas",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const getCabang = async (req, res) => {
  try {
    const id_cabang = req.params.id_cabang;
    const getTugas = await Tugas.findAll({
      where: {
        id_cabang: id_cabang,
        is_aktif: true,
      },
    });
    if (getTugas) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data tugas",
        result: getTugas,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data tugas",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const id_tugas = req.params.id_tugas;
    const getTugas = await tugasHelper.deleteTugas(id_tugas, {
      transaction: t,
    });

    await t.commit();
    if (getTugas) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data tugas",
        result: getTugas,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data tugas",
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  getCabang,
};
