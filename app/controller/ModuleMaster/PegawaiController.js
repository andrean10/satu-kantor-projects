const {
  pegawaiHelper,
  userHelper,
  userMappingHelper,
  pegawaiHelper: { getPegawaiById, getPegawai },
  cabangHelper,
  rekeningHelper,
  izinHelper,
  pagination,
  unitHelper,
  jabatanHelper,
  komponenGajiHelper,
  pegawaiKomponenGajiHelper,
  jabatanPegawaiHelper,
  unitPegawaiHelper,
  pegawaiKontrakHelper,
} = require("../../helper/index");
const {
  Cabang,
  Izin,
  Rekening,
  Jabatan,
  PegawaiJabatan,
  PegawaiKontrak,
  PegawaiKomponenGaji,
  PegawaiUnit,
  Unit,
  KomponenGaji,
  Users,
  UserMapping,
  Pegawai,
} = require("../../model");

const moment = require("moment");
const { validationResult } = require("express-validator");
const mv = require("mv");
const fs = require("fs");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const readXlsxFile = require("read-excel-file/node");
const { Op } = require("sequelize");
const sequelize = require("../../config/db");

const index = async (req, res) => {
  try {
    let user = req.user.data;

    if (req.xhr) {
      let getFilter = req.query.filter;

      let id_client = req.query.client_id;
      if (user.jenis_mapping == "client") {
        id_client = user.id_mapping;
      }
      // page
      const page =
        req.query.page == null || req.query.page == "" ? 1 : req.query.page;
      const limit =
        req.query.limit == null || req.query.limit == "" ? 10 : req.query.limit;
      const search = req.query.search;

      const halamanAkhir = page * limit;
      const halamanAwal = halamanAkhir - limit;
      const offset = halamanAkhir;
      const skip = halamanAwal;

      let pegawai = await pegawaiHelper.getPegawai(
        limit,
        skip,
        null,
        id_client,
        getFilter
      );
      let model = await pegawaiHelper.Pegawai.count({
        where: {
          id_client: user.id_mapping,
        },
      });
      if (getFilter != null) {
        let getModel = await getPegawai(null, null, null, id_client, getFilter);
        model = getModel.length;
      }

      if (search != null && search != "") {
        pegawai = await pegawaiHelper.getPegawai(
          limit,
          skip,
          search,
          id_client,
          getFilter
        );
        let getModel = await pegawaiHelper.getPegawai(
          null,
          null,
          search,
          id_client,
          getFilter
        );
        model = getModel.length;
      }

      // pagination
      const getPagination = pagination(page, model, limit);

      let keterangan = {
        from: skip + 1,
        to: offset,
        total: model,
      };

      let output = {
        data: pegawai,
        pagination: getPagination,
        keterangan: keterangan,
      };
      return res.status(200).json({
        status: 200,
        message: "Berhasil tangkap data",
        output: output,
      });
    }
    let id_client = req.query.client_id;
    if (user.jenis_mapping == "client") {
      id_client = user.id_mapping;
    }

    // breadcrumb
    let breadcrumb = [];
    if (user.jenis_mapping == "admin") {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Client",
        url: "/admin/client",
      });
      breadcrumb.push({
        label: "Dashboard Client",
        url: "/admin/dashboardClient?client_id=" + id_client,
      });
      breadcrumb.push({
        label: "Pegawai",
        url: "/admin/pegawai?client_id=" + id_client,
        isActive: "active",
      });
    } else {
      breadcrumb.push({ label: "Home", url: "/admin/dashboard", isActive: "" });
      breadcrumb.push({
        label: "Pegawai",
        url: "/admin/pegawai",
        isActive: "active",
      });
    }
    let cabang = await cabangHelper.Cabang.findAll({
      where: {
        id_client: id_client,
      },
    });

    let unit = await unitHelper.Unit.findAll({
      where: {
        id_client: id_client,
      },
      include: [
        {
          model: Jabatan,
          required: true,
        },
      ],
    });
    let komponenGajiKaryawan = await komponenGajiHelper.KomponenGaji.findAll({
      where: {
        id_client: id_client,
        komponen: sequelize.where(
          sequelize.fn("lower", sequelize.col("komponen")),
          sequelize.fn("lower", "karyawan")
        ),
      },
    });
    let komponenGajiPerusahaan = await komponenGajiHelper.KomponenGaji.findAll({
      where: {
        id_client: id_client,
        komponen: sequelize.where(
          sequelize.fn("lower", sequelize.col("komponen")),
          sequelize.fn("lower", "perusahaan")
        ),
      },
    });
    let pegawaiKomponenGaji = await komponenGajiHelper.KomponenGaji.findAll({
      where: {
        id_client: id_client,
      },
    });

    res.render("./moduleMaster/pegawai/index", {
      title: "Pegawai",
      breadcrumb: breadcrumb,
      currentUrl: req.originalUrl,
      id_client: id_client,
      client_id: id_client,
      cabang: cabang,
      unit: unit,
      komponenGajiKaryawan: komponenGajiKaryawan,
      komponenGajiPerusahaan: komponenGajiPerusahaan,
      pegawaiKomponenGaji: pegawaiKomponenGaji,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  let user = req.user.data;
  const t = await sequelize.transaction();

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    } else {
      const response = req.body;

      if (response.page == "add") {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

        let dataUser = {};
        dataUser.username = response.username;
        dataUser.password_hash = bcrypt.hashSync(
          response.password_hash,
          saltRounds
        );
        dataUser.email = response.email;
        dataUser.status = 1;
        dataUser.created_at = dateTime;
        dataUser.updated_at = dateTime;

        let insertUser = await userHelper.Users.create(dataUser, {
          transaction: t,
        });
        insertUser = insertUser.id;
        // let insertUser = 1;

        let gambar_db = await uploadGambar(response.gambar);
        let tanggal_keluar =
          response.tanggal_keluar != ""
            ? moment(response.tanggal_keluar, "DD-MM-YYYY").format("YYYY-MM-DD")
            : null;

        let dataPegawai = {
          id_client: response.id_client,
          id_absensi: response.id_absensi,
          jenis_identitas: response.jenis_identitas,
          no_identitas: response.no_identitas,
          nama_lengkap: response.nama_lengkap,
          jenis_kelamin: response.jenis_kelamin,
          tanggal_lahir: moment(response.tanggal_lahir, "DD-MM-YYYY").format(
            "YYYY-MM-DD"
          ),
          tempat_lahir: response.tempat_lahir,
          status_perkawinan: response.status_perkawinan,
          agama: response.agama,
          pendidikan: response.pendidikan,
          alamat_domisili: response.alamat_domisili,
          alamat_ktp: response.alamat_ktp,
          no_kontak1: response.no_kontak1,
          no_kontak2: response.no_kontak2,
          email: response.email,
          no_pegawai: response.no_pegawai,
          tanggal_masuk: moment(response.tanggal_masuk, "DD-MM-YYYY").format(
            "YYYY-MM-DD"
          ),
          tanggal_keluar: tanggal_keluar,
          is_aktif: 1,
          user_create: user.id,
          user_update: user.id,
          time_create: dateTime,
          time_update: dateTime,
          gambar: gambar_db,
        };

        let insertPegawai = await pegawaiHelper.Pegawai.create(dataPegawai, {
          transaction: t,
        });
        insertPegawai = insertPegawai.id_pegawai;
        // let insertPegawai = 1;

        let dataMappingUser = {};
        dataMappingUser.jenis_mapping = "pegawai";
        dataMappingUser.id_user = insertUser;
        dataMappingUser.id_mapping = insertPegawai;

        // let insertMappingUser = 1;
        let insertMappingUser = await userMappingHelper.UserMapping.create(
          dataMappingUser,
          {
            transaction: t,
          }
        );
        insertMappingUser = insertMappingUser.id_user_mapping;

        let rekening = {};
        rekening.nama_bank_rekening = response.nama_bank_rekening;
        rekening.nomor_rekening = response.nomor_rekening;
        rekening.pemilik_rekening = response.pemilik_rekening;
        rekening.pegawai_id = insertPegawai;

        let insertRekening = await rekeningHelper.insertRekening(rekening, {
          transaction: t,
        });
        insertRekening = insertRekening.id;
        // let insertRekening = 1;

        let tanggalKadaluarsaIzin =
          response.tanggal_kadaluarsa_izin != null &&
          response.tanggal_kadaluarsa_izin != ""
            ? moment(response.tanggal_kadaluarsa_izin, "DD-MM-YYYY").format(
                "YYYY-MM-DD"
              )
            : null;
        let izin = {};
        izin.str_izin = response.str_izin;
        izin.jenis_izin = response.jenis_izin;
        izin.nomor_izin = response.nomor_izin;
        izin.tanggal_kadaluarsa_izin = tanggalKadaluarsaIzin;
        izin.pegawai_id = insertPegawai;

        // let insertIzin = 1;
        let insertIzin = await izinHelper.insertIzin(izin, {
          transaction: t,
        });
        insertIzin = insertIzin.id;

        let dataPegawaiJabatan = {};
        let getDataJabatan = await jabatanHelper.getJabatanById(
          response.id_jabatan
        );
        let membawahi_jabatan = getDataJabatan.membawahi_jabatan;

        dataPegawaiJabatan.id_pegawai = insertPegawai;
        dataPegawaiJabatan.id_jabatan = response.id_jabatan;
        dataPegawaiJabatan.id_cabang = response.id_cabang;
        dataPegawaiJabatan.membawahi_jabatan = membawahi_jabatan;
        dataPegawaiJabatan.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiJabatan.tanggal_selesai = tanggal_keluar;
        dataPegawaiJabatan.is_aktif = response.is_aktif;
        dataPegawaiJabatan.user_create = user.id;
        dataPegawaiJabatan.user_update = user.id;
        dataPegawaiJabatan.time_create = dateTime;
        dataPegawaiJabatan.time_update = dateTime;

        // let insertPegawaiJabatan = 1;
        let insertPegawaiJabatan =
          await jabatanPegawaiHelper.PegawaiJabatan.create(dataPegawaiJabatan, {
            transaction: t,
          });
        insertPegawaiJabatan = insertPegawaiJabatan.id_pegawai_jabatan;

        let dataPegawaiUnit = {};
        let getDataUnit = await unitHelper.getUnitById(response.id_unit);
        let membawahi_unit = getDataUnit.membawahi_unit;

        dataPegawaiUnit.id_pegawai = insertPegawai;
        dataPegawaiUnit.id_unit = response.id_unit;
        dataPegawaiUnit.id_cabang = response.id_cabang;
        dataPegawaiUnit.membawahi_unit = membawahi_unit;
        dataPegawaiUnit.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiUnit.tanggal_selesai = tanggal_keluar;
        dataPegawaiUnit.is_aktif = response.is_aktif;
        dataPegawaiUnit.user_create = user.id;
        dataPegawaiUnit.user_update = user.id;
        dataPegawaiUnit.time_create = dateTime;
        dataPegawaiUnit.time_update = dateTime;

        // let insertPegawaiUnit = 1;
        let insertPegawaiUnit = await unitPegawaiHelper.PegawaiUnit.create(
          dataPegawaiUnit,
          {
            transaction: t,
          }
        );
        insertPegawaiUnit = insertPegawaiUnit.id_pegawai_unit;

        let dataPegawaiKontrak = {};
        let berkas = null;
        dataPegawaiKontrak.id_pegawai = insertPegawai;
        dataPegawaiKontrak.id_cabang = response.id_cabang;
        dataPegawaiKontrak.id_unit = response.id_unit;
        dataPegawaiKontrak.id_jabatan = response.id_jabatan;
        dataPegawaiKontrak.jenis_kontrak = response.jenis_kontrak;
        dataPegawaiKontrak.tanggal_mulai = moment(
          response.tanggal_mulai,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        let tanggalSelesai =
          response.tanggal_selesai != null && response.tanggal_selesai != ""
            ? moment(response.tanggal_selesai, "DD-MM-YYYY").format(
                "YYYY-MM-DD"
              )
            : null;
        dataPegawaiKontrak.tanggal_selesai = tanggalSelesai;
        dataPegawaiKontrak.berkas = berkas;
        dataPegawaiKontrak.is_aktif = response.is_aktif;
        dataPegawaiKontrak.user_create = user.id;
        dataPegawaiKontrak.user_update = user.id;
        dataPegawaiKontrak.time_create = dateTime;
        dataPegawaiKontrak.time_update = dateTime;

        let insertPegawaiKontrak =
          await pegawaiKontrakHelper.PegawaiKontrak.create(dataPegawaiKontrak, {
            transaction: t,
          });
        insertPegawaiKontrak = insertPegawaiKontrak.id_pegawai_kontrak;
        // let insertPegawaiKontrak = 1;

        let id_komponen_gaji = response.id_komponen_gaji;
        let insertPegawaiKomponenGaji = null;
        if (id_komponen_gaji != null && id_komponen_gaji != "") {
          id_komponen_gaji = id_komponen_gaji.split(",");
          let nominal = response.nominal;
          nominal = nominal.split(",");
          let dataKomponenGaji = [];
          id_komponen_gaji.map((v, i) => {
            dataKomponenGaji.push({
              id_pegawai_kontrak: insertPegawaiKontrak,
              id_komponen_gaji: v,
              nominal: nominal[i],
            });
          });

          insertPegawaiKomponenGaji =
            await pegawaiKomponenGajiHelper.insertBatch(dataKomponenGaji, {
              transaction: t,
            });
        }

        await t.commit();

        if (
          insertUser ||
          insertPegawai ||
          insertMappingUser ||
          insertRekening ||
          insertIzin ||
          insertPegawaiJabatan ||
          insertPegawaiUnit ||
          insertPegawaiKontrak ||
          insertPegawaiKomponenGaji
        ) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil insert data",
            result: response,
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal insert data",
          });
        }
      } else {
        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
        const getMapping = await userMappingHelper.UserMapping.findOne({
          where: {
            id_mapping: response.id_pegawai,
          },
        });
        const getUsers = await userHelper.Users.findOne({
          where: {
            id: getMapping.id_user,
          },
        });

        // users
        let dataUser = {};

        dataUser.username = response.username;
        let password_db = response.password_hash_old;
        let password = response.password_hash;
        if (password != null) {
          password_db = bcrypt.hashSync(password, saltRounds);
        }
        dataUser.password_hash = password_db;
        dataUser.email = response.email;
        dataUser.status = 1;
        dataUser.updated_at = dateTime;

        let updateUsers = await userHelper.Users.update(dataUser, {
          where: {
            id: getUsers.id,
          },
          transaction: t,
        });

        // pegawai
        let id_pegawai = response.id_pegawai;
        let gambar_db = await uploadGambar(response.gambar, id_pegawai);
        let pegawai = pegawaiHelper.getPegawaiById(id_pegawai);
        let tanggal_keluar =
          response.tanggal_keluar != ""
            ? moment(response.tanggal_keluar, "DD-MM-YYYY").format("YYYY-MM-DD")
            : pegawai.tanggal_keluar;

        let dataPegawai = {
          id_client: response.id_client,
          id_absensi: response.id_absensi,
          jenis_identitas: response.jenis_identitas,
          no_identitas: response.no_identitas,
          nama_lengkap: response.nama_lengkap,
          jenis_kelamin: response.jenis_kelamin,
          tanggal_lahir: moment(response.tanggal_lahir, "DD-MM-YYYY").format(
            "YYYY-MM-DD"
          ),
          tempat_lahir: response.tempat_lahir,
          status_perkawinan: response.status_perkawinan,
          agama: response.agama,
          pendidikan: response.pendidikan,
          alamat_domisili: response.alamat_domisili,
          alamat_ktp: response.alamat_ktp,
          no_kontak1: response.no_kontak1,
          no_kontak2: response.no_kontak2,
          email: response.email,
          no_pegawai: response.no_pegawai,
          tanggal_masuk: moment(response.tanggal_masuk, "DD-MM-YYYY").format(
            "YYYY-MM-DD"
          ),
          tanggal_keluar: tanggal_keluar,
          is_aktif: response.is_aktif,
          user_update: 1,
          time_update: dateTime,
          gambar: gambar_db,
        };

        let updatePegawai = await pegawaiHelper.Pegawai.update(dataPegawai, {
          where: {
            id_pegawai: id_pegawai,
          },
          transaction: t,
        });

        // rekening
        let rekening = {};
        rekening.nama_bank_rekening = response.nama_bank_rekening;
        rekening.nomor_rekening = response.nomor_rekening;
        rekening.pemilik_rekening = response.pemilik_rekening;
        rekening.pegawai_id = id_pegawai;

        let updateRekening = await rekeningHelper.updateRekening(
          rekening,
          id_pegawai,
          {
            transaction: t,
          }
        );

        // izin
        let tanggalKadaluarsaIzin =
          response.tanggal_kadaluarsa_izin != null &&
          response.tanggal_kadaluarsa_izin != ""
            ? moment(response.tanggal_kadaluarsa_izin, "DD-MM-YYYY").format(
                "YYYY-MM-DD"
              )
            : null;
        let izin = {};
        izin.str_izin = response.str_izin;
        izin.jenis_izin = response.jenis_izin;
        izin.nomor_izin = response.nomor_izin;
        izin.tanggal_kadaluarsa_izin = tanggalKadaluarsaIzin;
        izin.pegawai_id = id_pegawai;

        let updateIzin = await izinHelper.updateIzin(izin, id_pegawai, {
          transaction: t,
        });

        // pegawai jabaan
        let dataPegawaiJabatan = {};
        let getDataJabatan = await jabatanHelper.getJabatanById(
          response.id_jabatan
        );
        let membawahi_jabatan = getDataJabatan.membawahi_jabatan;

        dataPegawaiJabatan.id_pegawai = id_pegawai;
        dataPegawaiJabatan.id_jabatan = response.id_jabatan;
        dataPegawaiJabatan.id_cabang = response.id_cabang;
        dataPegawaiJabatan.membawahi_jabatan = membawahi_jabatan;
        dataPegawaiJabatan.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiJabatan.tanggal_selesai = tanggal_keluar;
        dataPegawaiJabatan.is_aktif = response.is_aktif;
        dataPegawaiJabatan.user_create = user.id;
        dataPegawaiJabatan.user_update = user.id;
        dataPegawaiJabatan.time_create = dateTime;
        dataPegawaiJabatan.time_update = dateTime;

        // let insertPegawaiJabatan = 1;
        let updatePegawaiJabatan =
          await jabatanPegawaiHelper.PegawaiJabatan.update(dataPegawaiJabatan, {
            where: {
              id_pegawai: id_pegawai,
              is_aktif: 1,
            },
            transaction: t,
          });
        updatePegawaiJabatan = updatePegawaiJabatan.id_pegawai_jabatan;

        // pegawai unit
        let dataPegawaiUnit = {};
        let getDataUnit = await unitHelper.getUnitById(response.id_unit);
        let membawahi_unit = getDataUnit.membawahi_unit;

        dataPegawaiUnit.id_pegawai = id_pegawai;
        dataPegawaiUnit.id_unit = response.id_unit;
        dataPegawaiUnit.id_cabang = response.id_cabang;
        dataPegawaiUnit.membawahi_unit = membawahi_unit;
        dataPegawaiUnit.tanggal_mulai = moment(
          response.tanggal_masuk,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");
        dataPegawaiUnit.tanggal_selesai = tanggal_keluar;
        dataPegawaiUnit.is_aktif = response.is_aktif;
        dataPegawaiUnit.user_create = user.id;
        dataPegawaiUnit.user_update = user.id;
        dataPegawaiUnit.time_create = dateTime;
        dataPegawaiUnit.time_update = dateTime;

        // let insertPegawaiUnit = 1;
        let updatePegawaiUnit = await unitPegawaiHelper.PegawaiUnit.update(
          dataPegawaiUnit,
          {
            where: {
              id_pegawai: id_pegawai,
              is_aktif: 1,
            },
            transaction: t,
          }
        );
        updatePegawaiUnit = updatePegawaiUnit.id_pegawai_unit;

        // pegawai kontrak
        let dataPegawaiKontrak = {};
        let berkas = null;
        dataPegawaiKontrak.id_pegawai = id_pegawai;
        dataPegawaiKontrak.id_cabang = response.id_cabang;
        dataPegawaiKontrak.id_unit = response.id_unit;
        dataPegawaiKontrak.id_jabatan = response.id_jabatan;
        dataPegawaiKontrak.jenis_kontrak = response.jenis_kontrak;
        dataPegawaiKontrak.tanggal_mulai = moment(
          response.tanggal_mulai,
          "DD-MM-YYYY"
        ).format("YYYY-MM-DD");

        let tanggalSelesai =
          response.tanggal_selesai != null && response.tanggal_selesai != ""
            ? moment(response.tanggal_selesai, "DD-MM-YYYY").format(
                "YYYY-MM-DD"
              )
            : null;
        dataPegawaiKontrak.tanggal_selesai = tanggalSelesai;
        dataPegawaiKontrak.berkas = berkas;
        dataPegawaiKontrak.is_aktif = response.is_aktif;
        dataPegawaiKontrak.user_create = user.id;
        dataPegawaiKontrak.user_update = user.id;
        dataPegawaiKontrak.time_create = dateTime;
        dataPegawaiKontrak.time_update = dateTime;

        let updatePegawaiKontrak =
          await pegawaiKontrakHelper.PegawaiKontrak.update(dataPegawaiKontrak, {
            where: {
              id_pegawai: id_pegawai,
              is_aktif: 1,
            },
            transaction: t,
          });
        updatePegawaiKontrak = updatePegawaiKontrak.id_pegawai_kontrak;
        // let insertPegawaiKontrak = 1;

        // komponen gaji
        const getKomponenGajiCheck = await pegawaiHelper.Pegawai.findOne({
          where: { id_pegawai: id_pegawai },
          include: [
            {
              model: PegawaiKontrak,
              where: {
                is_aktif: 1,
              },
              required: true,
              include: [
                {
                  model: PegawaiKomponenGaji,
                },
              ],
            },
          ],
        });
        const countCheck =
          getKomponenGajiCheck.pegawai_kontrak.pegawai_komponen_gajis.length;

        const id_pegawai_kontrak =
          getKomponenGajiCheck.pegawai_kontrak.id_pegawai_kontrak;

        if (countCheck > 0) {
          await PegawaiKomponenGaji.destroy({
            where: {
              id_pegawai_kontrak: id_pegawai_kontrak,
            },
            transaction: t,
          });
        }

        let updatePegawaiKomponenGaji = null;
        if (
          response.id_komponen_gaji != null &&
          response.id_komponen_gaji != ""
        ) {
          let id_komponen_gaji = response.id_komponen_gaji;
          id_komponen_gaji = id_komponen_gaji.split(",");
          let nominal = response.nominal;
          nominal = nominal.split(",");
          let dataKomponenGaji = [];
          id_komponen_gaji.map((v, i) => {
            dataKomponenGaji.push({
              id_pegawai_kontrak: id_pegawai_kontrak,
              id_komponen_gaji: v,
              nominal: nominal[i],
            });
          });

          updatePegawaiKomponenGaji =
            await pegawaiKomponenGajiHelper.insertBatch(dataKomponenGaji, {
              transaction: t,
            });
        }

        await t.commit();

        if (
          updateUsers ||
          updatePegawai ||
          updateRekening ||
          updateIzin ||
          updatePegawaiJabatan ||
          updatePegawaiUnit ||
          updatePegawaiKontrak ||
          updatePegawaiKomponenGaji
        ) {
          return res.json({
            status: 200,
            message: "Berhasil update data",
            result: response,
          });
        } else {
          return res.json({
            status: 400,
            message: "Gagal update data",
          });
        }
      }
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const edit = async (req, res) => {
  try {
    const id_pegawai = req.params.id_pegawai;

    const getPegawai = await pegawaiHelper.Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
        "$pegawai_unit.is_aktif$": 1,
        "$pegawai_jabatan.is_aktif$": 1,
        "$pegawai_kontrak.is_aktif$": 1,
      },
      include: [
        {
          model: Rekening,
        },
        {
          model: Izin,
        },
        {
          model: PegawaiJabatan,
        },
        {
          model: PegawaiUnit,
        },
        {
          model: PegawaiKontrak,
          include: [
            {
              model: PegawaiKomponenGaji,
            },
          ],
        },
      ],
    });

    const getMapping = await userMappingHelper.UserMapping.findOne({
      where: {
        id_mapping: id_pegawai,
      },
    });
    const getUsers = await userHelper.Users.findOne({
      where: {
        id: getMapping.id_user,
      },
    });
    let output = {};
    output.pegawai = getPegawai;
    output.mapping = getMapping;
    output.users = getUsers;
    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const cabangPegawai = async (req, res) => {
  try {
    const id_cabang = req.params.id_cabang;
    const getCabang = await Cabang.findOne({
      where: {
        id_cabang: id_cabang,
      },
      include: [
        {
          model: Unit,
          required: true,
        },
      ],
    });

    if (getCabang) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data cabang",
        result: getCabang,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data cabang",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const unitPegawai = async (req, res) => {
  try {
    const id_unit = req.params.id_unit;
    const getUnit = await unitHelper.Unit.findOne({
      where: {
        id_unit: id_unit,
      },
      include: [
        {
          model: Jabatan,
          required: true,
        },
      ],
    });

    if (getUnit) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data unit",
        result: getUnit,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data unit",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const detail = async (req, res) => {
  try {
    const id_pegawai = req.params.id_pegawai;
    const getPegawai = await pegawaiHelper.Pegawai.findOne({
      where: {
        id_pegawai: id_pegawai,
        "$pegawai_jabatan.is_aktif$": 1,
        "$pegawai_unit.is_aktif$": 1,
        "$pegawai_kontrak.is_aktif$": 1,
      },
      include: [
        {
          model: Rekening,
          required: true,
        },
        {
          model: Izin,
          required: true,
        },
        {
          model: PegawaiJabatan,
          required: true,
          include: [
            {
              model: Jabatan,
              required: true,
            },
            {
              model: Cabang,
              required: true,
            },
          ],
        },
        {
          model: PegawaiUnit,
          required: true,
          include: [
            {
              model: Unit,
              required: true,
            },
          ],
        },
        {
          model: PegawaiKontrak,
          required: true,
          include: [
            {
              model: PegawaiKomponenGaji,
              include: [
                {
                  model: KomponenGaji,
                },
              ],
            },
          ],
        },
      ],
    });
    const getMapping = await userMappingHelper.UserMapping.findOne({
      where: {
        id_mapping: id_pegawai,
      },
    });
    const getUsers = await userHelper.Users.findOne({
      where: {
        id: getMapping.id_user,
      },
    });
    let output = {};
    output.pegawai = getPegawai;
    output.mapping = getMapping;
    output.users = getUsers;
    if (output) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil mengambil data pegawai",
        result: output,
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal mengambil data pegawai",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const deleteData = async (req, res) => {
  const t = await sequelize.transaction();

  try {
    const id_pegawai = req.params.id_pegawai;
    await deleteGambar(id_pegawai);
    const getMapping = await userMappingHelper.UserMapping.findOne({
      where: {
        id_mapping: id_pegawai,
      },
    });

    const getUsers = await userHelper.Users.findOne({
      where: {
        id: getMapping.id_user,
      },
    });
    const deletePegawai = await pegawaiHelper.Pegawai.destroy({
      where: {
        id_pegawai: id_pegawai,
      },
      transaction: t,
    });

    const deleteUser = await userHelper.Users.destroy({
      where: {
        id: getUsers.id,
      },
      transaction: t,
    });

    await t.commit();
    if (deletePegawai || deleteUser) {
      return res.status(200).json({
        status: 200,
        message: "Berhasil menghapus data pegawai",
      });
    } else {
      return res.status(400).json({
        status: 400,
        message: "Gagal menghapus data pegawai",
      });
    }
  } catch (error) {
    await t.rollback();
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const uploadGambar = async (responseGambar, id_pegawai = null) => {
  if (responseGambar.gambar != null) {
    const { gambar } = responseGambar;

    let pathOld = gambar.path;
    let name = gambar.name;
    let size = gambar.size;

    name =
      moment
        .duration(moment(moment().format("YYYY-MM-DD HH:mm:ss")))
        .asSeconds() +
      "_" +
      name.split(" ").join("-");
    pathNew = "public/image/pegawai/" + name;
    if (size > 0) {
      if (pathOld != null && pathNew != null) {
        if (id_pegawai != null) {
          await deleteGambar(id_pegawai);
        }

        mv(pathOld, pathNew, function (err) {
          if (err) {
            return err;
          }
        });

        const fileName = pathNew.split("/");
        const fileDb = fileName[fileName.length - 1];
        return fileDb;
      }
    }
  }

  if (id_pegawai != null) {
    let getPegawai = await getPegawaiById(id_pegawai);
    if (getPegawai.gambar != "default.png" && getPegawai.gambar != null) {
      return getPegawai.gambar;
    }
  }
  return "default.png";
};

const deleteGambar = async (id_pegawai = null) => {
  if (id_pegawai != null) {
    let getPegawai = await getPegawaiById(id_pegawai);
    if (getPegawai.gambar != "default.png" && getPegawai.gambar != null) {
      let unlink = "public/image/pegawai/" + getPegawai.gambar;
      if (fs.existsSync(unlink)) {
        fs.unlinkSync(unlink);
      }
    }
  }
};

const importData = async (req, res) => {
  let user = req.user.data;

  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }
    const { importData } = req.body.importData;

    readXlsxFile(importData.path).then(async (rows) => {
      let pushPegawai = [];
      let pushPegawaiKomponenGaji = [];
      let username = null;
      rows.map(async (v, i) => {
        if (i > 1) {
          if (v[1] != null && v[1] != "" && v[1] != "-") {
            pushPegawai.push(pushImportData(v, user));
          }

          if (v[34] != null && v[34] != "" && v[34] != "-") {
            let dataKomponenGaji = await KomponenGaji.findOne({
              where: {
                komponen_gaji: {
                  [Op.iLike]: "%" + v[34] + "%",
                },
                id_client: user.id_mapping,
              },
            });

            if (v[1] != null && v[1] != "" && v[1] != "-") {
              username = v[1];
            }

            let nominalKomponenGaji = v[35];
            pushPegawaiKomponenGaji.push({
              [username]: [
                {
                  id_pegawai_kontrak: null,
                  id_komponen_gaji: dataKomponenGaji.id_komponen_gaji,
                  nominal: nominalKomponenGaji,
                },
              ],
            });
          }
        }
      });

      // conditional
      Promise.all(pushPegawai).then(async (result) => {
        let data = [...result];
        let count = [];

        data.map(async (v, i) => {
          count.push(insertImportData(v, pushPegawaiKomponenGaji));
        });

        if (result.length > 0) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil import " + result.length + " data pegawai",
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal import " + result.length + " data pegawai",
          });
        }
      });
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const pushImportData = async (v, user) => {
  let pushData = [];
  let dataCabang = await Cabang.findOne({
    where: {
      nama_cabang: {
        [Op.iLike]: "%" + v[28] + "%",
      },
      id_client: user.id_mapping,
    },
  });

  let dataJabatan = await Jabatan.findOne({
    where: {
      nama_jabatan: {
        [Op.iLike]: "%" + v[30] + "%",
      },
      "$unit.cabang.id_cabang$": dataCabang.id_cabang,
      id_client: user.id_mapping,
    },
    include: [
      {
        model: Unit,
        include: [
          {
            model: Cabang,
          },
        ],
      },
    ],
  });

  let dataUnit = await Unit.findOne({
    where: {
      nama_unit: {
        [Op.iLike]: "%" + v[29] + "%",
      },
      id_client: user.id_mapping,
      id_cabang: dataCabang.id_cabang,
    },
  });

  let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
  pushData.push({
    user: {
      username: v[1],
      password_hash: bcrypt.hashSync(String(v[2]), saltRounds),
      email: v[3],
      status: 1,
      created_at: dateTime,
      updated_at: dateTime,
    },
    pegawai: {
      id_client: user.id_mapping,
      id_absensi: v[4],
      jenis_identitas: v[5].toLowerCase(),
      no_identitas: v[6],
      nama_lengkap: v[7],
      jenis_kelamin: v[8],
      tempat_lahir: v[9],
      tanggal_lahir: v[10] != 0 && v[10] != "-" && v[10] != "" ? v[10] : null,
      status_perkawinan: v[12].toLowerCase(),
      agama: v[13].toLowerCase(),
      pendidikan: v[11].toLowerCase(),
      alamat_domisili: v[14],
      alamat_ktp: v[15],
      no_kontak1: v[16],
      no_kontak2: v[17],
      email: v[3],
      no_pegawai: v[25],
      tanggal_masuk: v[26] != 0 && v[26] != "-" && v[26] != "" ? v[26] : null,
      tanggal_keluar: v[27] != 0 && v[27] != "-" && v[27] != "" ? v[27] : null,
      is_aktif: 1,
      user_create: user.id,
      user_update: user.id,
      time_create: dateTime,
      time_update: dateTime,
      gambar: "default.png",
    },
    rekening: {
      nama_bank_rekening:
        v[18] != null && v[18] != "-" && v[18] != "" ? v[18] : null,
      nomor_rekening:
        v[19] != null && v[19] != "-" && v[19] != "" ? v[19] : null,
      pemilik_rekening:
        v[20] != null && v[20] != "-" && v[20] != "" ? v[20] : null,
      pegawai_id: null,
    },
    izin: {
      str_izin: v[21] != null && v[21] != "-" && v[21] != "" ? v[21] : null,
      jenis_izin:
        v[22] != null && v[22] != "-" && v[22] != ""
          ? v[22].toLowerCase()
          : null,
      nomor_izin: v[23] != null && v[23] != "-" && v[23] != "" ? v[23] : null,
      tanggal_kadaluarsa_izin:
        v[24] != 0 && v[24] != "-" && v[24] != "" && v[24] != null
          ? v[24]
          : null,
      pegawai_id: null,
    },
    pegawai_jabatan: {
      id_pegawai: null,
      id_jabatan: dataJabatan.id_jabatan,
      id_cabang: dataCabang.id_cabang,
      membawahi_jabatan: dataJabatan.membawahi_jabatan,
      tanggal_mulai:
        v[26] != 0 && v[26] != "-" && v[26] != null && v[26] != ""
          ? v[26]
          : null,
      tanggal_selesai:
        v[27] != 0 && v[27] != "-" && v[27] != null && v[27] != ""
          ? v[27]
          : null,
      is_aktif: 1,
      user_create: user.id,
      user_update: user.id,
      time_create: dateTime,
      time_update: dateTime,
    },
    pegawai_unit: {
      id_pegawai: null,
      id_unit: dataUnit.id_unit,
      id_cabang: dataCabang.id_cabang,
      membawahi_unit: dataUnit.membawahi_unit,
      tanggal_mulai:
        v[26] != 0 && v[26] != "-" && v[26] != null && v[26] != ""
          ? v[26]
          : null,
      tanggal_selesai:
        v[27] != 0 && v[27] != "-" && v[27] != null && v[27] != ""
          ? v[27]
          : null,
      is_aktif: 1,
      user_create: user.id,
      user_update: user.id,
      time_create: dateTime,
      time_update: dateTime,
    },
    pegawai_kontrak: {
      id_pegawai: null,
      id_cabang: dataCabang.id_cabang,
      id_unit: dataUnit.id_unit,
      id_jabatan: dataJabatan.id_jabatan,
      jenis_kontrak: v[31].toLowerCase(),
      tanggal_mulai:
        v[32] != 0 && v[32] != "-" && v[32] != null && v[32] != ""
          ? v[32]
          : null,
      tanggal_selesai:
        v[33] != 0 && v[33] != "-" && v[33] != null && v[33] != ""
          ? v[33]
          : null,
      berkas: null,
      is_aktif: 1,
      user_create: user.id,
      user_update: user.id,
      time_create: dateTime,
      time_update: dateTime,
    },
  });
  return pushData;
};

const insertIndukData = async (user, pegawai) => {
  // insert user
  let insertUser = await Users.create(user);
  let id_user = insertUser.id;

  // insert pegawai
  let insertPegawai = await Pegawai.create(pegawai);
  let id_pegawai = insertPegawai.id_pegawai;

  // insert user mapping
  let insertUserMapping = await UserMapping.create({
    jenis_mapping: "pegawai",
    id_user: id_user,
    id_mapping: id_pegawai,
  });

  return id_pegawai;
};

const insertImportData = async (v, pushPegawaiKomponenGaji) => {
  let user = v[0].user;
  let username = user.username;
  let pegawai = v[0].pegawai;
  let rekening = v[0].rekening;
  let izin = v[0].izin;
  let pegawai_jabatan = v[0].pegawai_jabatan;
  let pegawai_unit = v[0].pegawai_unit;
  let pegawai_kontrak = v[0].pegawai_kontrak;

  // insert user
  let pushUser = [];
  pushUser.push(insertIndukData(user, pegawai));

  Promise.all(pushUser).then(async (result) => {
    let id_pegawai = result;

    let dbRekening = { ...rekening, pegawai_id: id_pegawai };
    let dbIzin = { ...izin, pegawai_id: id_pegawai };
    let dbPegawaiJabatan = { ...pegawai_jabatan, id_pegawai: id_pegawai };
    let dbPegawaiUnit = { ...pegawai_unit, id_pegawai: id_pegawai };
    let dbPegawaiKontrak = { ...pegawai_kontrak, id_pegawai: id_pegawai };

    // insert rekening
    let insertRekening = await Rekening.create(dbRekening);
    // insert izin
    let insertIzin = await Izin.create(dbIzin);
    // insert pegawai jabatan
    let insertPegawaiJabatan = await PegawaiJabatan.create(dbPegawaiJabatan);
    // insert pegawai unit
    let insertPegawaiUnit = await PegawaiUnit.create(dbPegawaiUnit);
    // insert pegawai kontrak
    let insertPegawaiKontrak = await PegawaiKontrak.create(dbPegawaiKontrak);

    let id_pegawai_kontrak = insertPegawaiKontrak.id_pegawai_kontrak;
    let dbPegawaiKomponenGaji = [];
    if (pushPegawaiKomponenGaji.length > 0) {
      pushPegawaiKomponenGaji.map((v, i) => {
        if (v[username] != undefined) {
          let arrPegawaiKontrak = {
            ...v[username][0],
            id_pegawai_kontrak: id_pegawai_kontrak,
          };
          dbPegawaiKomponenGaji.push(arrPegawaiKontrak);
        }
      });
    }
    if (dbPegawaiKomponenGaji.length > 0) {
      // insert pegawai komponen gaji
      let insertPegawaiKomponenGaji = await PegawaiKomponenGaji.bulkCreate(
        dbPegawaiKomponenGaji
      );
    }

    return id_pegawai;
  });
};

module.exports = {
  index,
  store,
  edit,
  deleteData,
  detail,
  unitPegawai,
  importData,
  cabangPegawai,
};
