const {
  userHelper: { Users, getUsersById, getUserAccount },
} = require("../helper/index");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
require("dotenv").config();
var LocalStorage = require("node-localstorage").LocalStorage;

const redirectUrl = (req, res) => {
  if (req.is_login) {
    let url = "/admin/dashboard";
    if (req.session.currentUrl != null) {
      url = req.session.currentUrl;
      if (url == "/admin/unit/getFindInUnit") {
        url = "/admin/unit";
      }
      if (url == "/admin/jabatan/getFindInJabatan") {
        url = "/admin/jabatan";
      }
    }
    res.redirect(url);
  }
};

const index = async (req, res) => {
  redirectUrl(req, res);

  try {
    localStorage = new LocalStorage("./scratch");
    // alert session
    const alert = {};
    alert.status = null;
    alert.message = null;

    let message = req.flash("error")[0];
    if (message != null) {
      alert.status = "error";
      alert.message = message;
    }

    message = req.flash("success")[0];
    if (message != null) {
      alert.status = "success";
      alert.message = message;
    }

    res.render("./login/index", {
      layout: "./layouts/home-user",
      title: "Login page",
      alert: alert,
    });
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

const store = async (req, res) => {
  try {
    const { username, password_hash, remember } = req.body;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        status: 400,
        message: "Invalid form validation",
        result: errors.array(),
      });
    }
    let checkUsername = await getUserAccount(username);

    if (checkUsername != null) {
      let hash = checkUsername.password_hash;
      let check = bcrypt.compareSync(password_hash, hash);
      if (check) {
        let jenis_mapping = checkUsername.user_mapping.jenis_mapping;
        let id_mapping = checkUsername.user_mapping.id_mapping;
        let id_user_mapping = checkUsername.user_mapping.id_user_mapping;

        let data = {};
        data.id = checkUsername.id;
        data.jenis_mapping = jenis_mapping;
        data.id_mapping = id_mapping;
        data.id_user_mapping = id_user_mapping;

        // json web token
        const token = jwt.sign(
          {
            data: data,
          },
          process.env.JWT_SECRET,
          { expiresIn: "30d" }
        );
        req.session.token = token;

        localStorage = new LocalStorage("./scratch");
        if (remember != null) {
          const auth_key = jwt.sign(
            {
              data: data,
            },
            process.env.JWT_SECRET,
            { expiresIn: "30d" }
          );
          req.session.auth_key = auth_key;
          localStorage.setItem("cookie", auth_key);
        }

        // local storage
        localStorage.setItem("auth", token);

        if (token) {
          return res.status(200).json({
            status: 200,
            message: "Berhasil login",
          });
        } else {
          return res.status(400).json({
            status: 400,
            message: "Gagal login",
          });
        }
      } else {
        return res.status(400).json({
          status: 400,
          message: "Password salah",
        });
      }
    } else {
      return res.status(400).json({
        status: 400,
        message: "Username tidak ditemukan",
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: error.message,
    });
  }
};

const logout = async (req, res) => {
  try {
    localStorage = new LocalStorage("./scratch");

    localStorage.removeItem("auth");
    localStorage.removeItem("cookie");

    req.flash("success", "Berhasil logout");
    res.redirect("/login");
  } catch (error) {
    return res.status(400).json({
      status: 400,
      message: "Terjadi kesalahan data",
      result: error.message,
    });
  }
};

module.exports = {
  index,
  store,
  logout,
};
