const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const { Pegawai } = require("./index");
const moment = require("moment");

const Rainburstment = sequilize.define(
  "rainburstment",
  {
    id_rainburstment: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    tanggal: {
      type: DataTypes.DATEONLY,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_create = this.getDataValue("time_create");
        let varMoment = time_create;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_create")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        let time_update = this.getDataValue("time_update");
        let varMoment = time_update;
        if (varMoment != null) {
          varMoment = moment(this.getDataValue("time_update")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return varMoment;
      },
    },
    keterangan: {
      type: DataTypes.TEXT,
    },
    process: {
      type: DataTypes.STRING,
    },
    total_rainburstment: {
      type: DataTypes.FLOAT,
      defaultValue: null,
      allowNull: true,
    },
    struk_pembelian: {
      type: DataTypes.STRING,
    },
    jumlah_diterima: {
      type: DataTypes.FLOAT,
      defaultValue: null,
      allowNull: true,
    },
    kembalian_diterima: {
      type: DataTypes.FLOAT,
      defaultValue: null,
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "rainburstment",
  }
);

module.exports = { Rainburstment };
