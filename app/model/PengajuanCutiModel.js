const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const moment = require("moment");
const { JenisCuti, Pegawai } = require("../model");

const PengajuanCuti = sequelize.define(
  "pengajuan_cuti",
  {
    id_pengajuan_cuti: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_jenis_cuti: {
      type: DataTypes.INTEGER,
      references: {
        model: JenisCuti,
        key: "id_jenis_cuti",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    tanggal_awal_cuti: {
      type: DataTypes.DATEONLY,
    },
    tanggal_akhir_cuti: {
      type: DataTypes.DATEONLY,
    },
    id_pegawai_pengganti: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai_pengganti",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    catatan: {
      type: DataTypes.TEXT,
    },
    id_atasan: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_atasan",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    is_setuju: {
      type: DataTypes.INTEGER,
    },
    waktu_pengajuan: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("waktu_pengajuan")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    waktu_setuju: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        if (this.getDataValue("waktu_setuju") != null) {
          return moment(this.getDataValue("waktu_setuju")).format(
            "YYYY-MM-DD HH:mm:ss"
          );
        }
        return null;
      },
    },
    berkas: {
      type: DataTypes.STRING,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_create")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_update")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
  },
  {
    timestamps: false,
    tableName: "pengajuan_cuti",
  }
);

module.exports = { PengajuanCuti };
