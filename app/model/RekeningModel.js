const { DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const { Pegawai } = require("./index");

const Rekening = sequelize.define(
  "rekening",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nama_bank_rekening: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    nomor_rekening: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    pemilik_rekening: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    pegawai_id: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
  },
  {
    timestamps: false,
    tableName: "rekening",
  }
);

module.exports = { Rekening };
