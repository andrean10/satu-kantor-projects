const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const moment = require("moment");
const { Client, Cabang, JenisLembur } = require(".");

const PengajuanLembur = sequelize.define(
  "pengajuan_lembur",
  {
    id_pengajuan_lembur: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_client: {
      type: DataTypes.INTEGER,
      references: {
        model: Client,
        key: "id_client",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_cabang: {
      type: DataTypes.INTEGER,
      references: {
        model: Cabang,
        key: "id_cabang",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_jenis_lembur: {
      type: DataTypes.INTEGER,
      references: {
        model: JenisLembur,
        key: "id_jenis_lembur",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    tanggal_lembur: {
      type: DataTypes.DATEONLY,
    },
    waktu_mulai: {
      type: DataTypes.TIME,
    },
    waktu_selesai: {
      type: DataTypes.TIME,
    },
    catatan: {
      type: DataTypes.TEXT,
    },
    berkas: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    is_delete: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_create")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_update")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
  },
  {
    timestamps: false,
    tableName: "pengajuan_lembur",
  }
);

module.exports = { PengajuanLembur };
