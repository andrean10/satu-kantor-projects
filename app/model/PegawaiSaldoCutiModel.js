const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequilize = require("../config/db");
const { JenisCuti, Pegawai } = require("./index");
const moment = require("moment");

const PegawaiSaldoCuti = sequilize.define(
  "pegawai_saldo_cuti",
  {
    id_pegawai_saldo_cuti: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_jenis_cuti: {
      type: DataTypes.INTEGER,
      references: {
        model: JenisCuti,
        key: "id_jenis_cuti",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    periode_awal: {
      type: DataTypes.DATEONLY,
    },
    periode_akhir: {
      type: DataTypes.DATEONLY,
    },
    saldo: {
      type: DataTypes.INTEGER,
    },
    is_aktif: {
      type: DataTypes.INTEGER,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_create")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_update")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    tableName: "pegawai_saldo_cuti",
  }
);

module.exports = { PegawaiSaldoCuti };
