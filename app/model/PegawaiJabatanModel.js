const { Sequelize, DataTypes, Op, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const { Pegawai, Jabatan, Cabang } = require("./index");
const moment = require("moment");

const PegawaiJabatan = sequelize.define(
  "pegawai_jabatan",
  {
    id_pegawai_jabatan: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_pegawai: {
      type: DataTypes.INTEGER,
      references: {
        model: Pegawai,
        key: "id_pegawai",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_jabatan: {
      type: DataTypes.INTEGER,
      references: {
        model: Jabatan,
        key: "id_jabatan",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_cabang: {
      type: DataTypes.INTEGER,
      references: {
        model: Cabang,
        key: "id_cabang",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    membawahi_jabatan: {
      type: DataTypes.STRING,
    },
    tanggal_mulai: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    tanggal_selesai: {
      type: DataTypes.DATEONLY,
      allowNull: true,
    },
    is_aktif: {
      type: DataTypes.INTEGER,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_create")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_update")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
  },
  {
    timestamps: false,
    tableName: "pegawai_jabatan",
  }
);

module.exports = { PegawaiJabatan };
