const { Sequelize, DataTypes, Deferrable } = require("sequelize");
const sequelize = require("../config/db");
const moment = require("moment");
const { Client, Cabang } = require("../model");

const PengajuanGaji = sequelize.define(
  "pengajuan_gaji",
  {
    id_pengajuan_gaji: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    id_client: {
      type: DataTypes.INTEGER,
      references: {
        model: Client,
        key: "id_client",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    id_cabang: {
      type: DataTypes.INTEGER,
      references: {
        model: Cabang,
        key: "id_cabang",
        deferrable: Deferrable.INITIALLY_IMMEDIATE,
      },
    },
    periode_pengajuan: {
      type: DataTypes.DATEONLY,
    },
    status_pengajuan: {
      type: DataTypes.STRING,
    },
    is_delete: {
      type: DataTypes.INTEGER,
    },
    user_create: {
      type: DataTypes.INTEGER,
    },
    user_update: {
      type: DataTypes.INTEGER,
    },
    time_create: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_create")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    time_update: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: true,
      get() {
        return moment(this.getDataValue("time_update")).format(
          "YYYY-MM-DD HH:mm:ss"
        );
      },
    },
    periode_awal: {
      type: DataTypes.DATEONLY,
    },
    periode_akhir: {
      type: DataTypes.DATEONLY,
    },
  },
  {
    timestamps: false,
    tableName: "pengajuan_gaji",
  }
);

module.exports = { PengajuanGaji };
