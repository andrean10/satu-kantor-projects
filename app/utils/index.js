const randomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
const randomOtpNumber = () => {
  let random1 = randomNumber(0, 9);
  let random2 = randomNumber(0, 9);
  let random3 = randomNumber(0, 9);
  let random4 = randomNumber(0, 9);
  let random5 = randomNumber(0, 9);
  let random6 = randomNumber(0, 9);
  let random =
    String(random1) +
    String(random2) +
    String(random3) +
    String(random4) +
    String(random5) +
    String(random6);

  return 123456;
  // return random;
};

const convertBulanToNumber = (bulan) => {
  let getBulan = bulan.toLowerCase();
  switch (getBulan) {
    case "januari":
      return "01";
    case "februari":
      return "02";
    case "maret":
      return "03";
    case "april":
      return "04";
    case "mei":
      return "05";
    case "juni":
      return "06";
    case "juli":
      return "07";
    case "agustus":
      return "08";
    case "september":
      return "09";
    case "oktober":
      return "10";
    case "november":
      return "11";
    case "desember":
      return "12";
  }
};

module.exports = {
  randomNumber,
  randomOtpNumber,
  convertBulanToNumber,
};
