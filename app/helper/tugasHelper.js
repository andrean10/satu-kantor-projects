const { Op, Sequelize } = require("sequelize");
const { Tugas, Cabang, Client } = require("../model/index");
const moment = require("moment");
const sequelize = require("../config/db");

const getTugas = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_client != "" && filter.id_client != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_client: filter.id_client,
    };
  }
  if (filter.id_cabang != "" && filter.id_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_cabang: filter.id_cabang,
    };
  }
  if (filter.nama_tugas != "" && filter.nama_tugas != null) {
    whereObj.filter = {
      ...whereObj.filter,
      nama_tugas: {
        [Op.iLike]: "%" + filter.nama_tugas + "%",
      },
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  let tugas = "";

  // execute
  let pushWhere = [];
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        nama_tugas: {
          [Op.iLike]: "%" + search + "%",
        },
        "$cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Cabang,
    },
    {
      model: Client,
    },
  ];
  tugas = await Tugas.findAll({
    order: [["id_tugas", "asc"]],
    where: pushWhere,
    include: getInclude,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    tugas = await Tugas.findAll({
      order: [["id_tugas", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return tugas;
};

const getTugasAll = async () => {
  let tugas = await Tugas.findAll();
  return tugas;
};

const getTugasId = async (id_tugas = null) => {
  let tugas = null;
  let getInclude = [
    {
      model: Cabang,
    },
    {
      model: Client,
    },
  ];
  if (id_tugas != null) {
    tugas = await Tugas.findOne({
      where: {
        id_tugas: id_tugas,
      },
      include: getInclude,
    });
  }
  return tugas;
};

const createTugas = async (data = {}, transaction = {}) => {
  let insert = await Tugas.create(data, transaction);
  return insert;
};

const updateTugas = async (data = {}, id_tugas, transaction = {}) => {
  let update = await Tugas.update(data, {
    where: {
      id_tugas: id_tugas,
    },
    ...transaction,
  });
  return update;
};

const deleteTugas = async (id_tugas = null, transaction = {}) => {
  let tugas = await Tugas.destroy({
    where: {
      id_tugas: id_tugas,
    },
    ...transaction,
  });
  return tugas;
};

module.exports = {
  getTugasAll,
  getTugas,
  getTugasId,
  createTugas,
  updateTugas,
  deleteTugas,
  Tugas,
};
