const { Op } = require("sequelize");
const { Penugasan, PenugasanStatus } = require("../model/index");

const getPenugasanStatus = async (
  limit = null,
  offset = null,
  search = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_penugasan != "" && filter.id_penugasan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_penugasan: filter.id_penugasan,
    };
  }
  if (filter.status != "" && filter.status != null) {
    whereObj.filter = {
      ...whereObj.filter,
      status: {
        [Op.iLike]: "%" + filter.status + "%",
      },
    };
  }
  if (filter.keterangan != "" && filter.keterangan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      keterangan: {
        [Op.iLike]: "%" + filter.keterangan + "%",
      },
    };
  }

  let penugasan_status = "";

  // execute
  let pushWhere = [];
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        status: {
          [Op.iLike]: "%" + search + "%",
        },
        keterangan: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Penugasan,
    },
  ];

  penugasan_status = await PenugasanStatus.findAll({
    order: [["id_penugasan_status", "asc"]],
    where: pushWhere,
    include: getInclude,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    penugasan_status = await PenugasanStatus.findAll({
      order: [["id_penugasan_status", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return penugasan_status;
};

const getPenugasanStatusAll = async () => {
  let penugasan_status = await PenugasanStatus.findAll();
  return penugasan_status;
};

const getPenugasanStatusId = async (id_penugasan_status = null) => {
  let penugasan_status = null;
  if (id_penugasan_status != null) {
    penugasan_status = await PenugasanStatus.findOne({
      where: {
        id_penugasan_status: id_penugasan_status,
      },
      include: [
        {
          model: Penugasan,
        },
      ],
    });
  }
  return penugasan_status;
};

const createPenugasanStatus = async (data = {}) => {
  let insert = await PenugasanStatus.create(data);
  return insert;
};

const updatePenugasanStatus = async (
  data = {},
  id_penugasan_status,
  transaction
) => {
  let update = await PenugasanStatus.update(data, {
    where: {
      id_penugasan_status: id_penugasan_status,
    },
    ...transaction,
  });
  return update;
};

const deletePenugasanStatus = async (id_penugasan_status = null) => {
  let penugasan_status = await PenugasanStatus.destroy({
    where: {
      id_penugasan_status: id_penugasan_status,
    },
  });
  return penugasan_status;
};

module.exports = {
  getPenugasanStatus,
  getPenugasanStatusAll,
  getPenugasanStatusId,
  createPenugasanStatus,
  updatePenugasanStatus,
  deletePenugasanStatus,
  Penugasan,
};
