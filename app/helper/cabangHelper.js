const { Cabang } = require("../model/index");
const { Op } = require("sequelize");

const getCabang = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.nama_cabang != "" && filter.nama_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      nama_cabang: {
        [Op.iLike]: "%" + filter.nama_cabang + "%",
      },
    };
  }
  if (filter.alamat != "" && filter.alamat != null) {
    whereObj.filter = {
      ...whereObj.filter,
      alamat: {
        [Op.iLike]: "%" + filter.alamat + "%",
      },
    };
  }
  if (filter.kontak != "" && filter.kontak != null) {
    whereObj.filter = {
      ...whereObj.filter,
      kontak: filter.kontak,
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let cabang = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        nama_cabang: {
          [Op.iLike]: "%" + search + "%",
        },
        alamat: {
          [Op.iLike]: "%" + search + "%",
        },
        kontak: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  cabang = await Cabang.findAll({
    order: [["id_cabang", "asc"]],
    limit: limit,
    offset: offset,
    where: pushWhere,
  });

  if (limit == null && offset == null) {
    cabang = await Cabang.findAll({
      order: [["id_cabang", "asc"]],
      where: pushWhere,
    });
  }

  return cabang;
};

const getCabangById = async (id_cabang = null) => {
  let cabang = "";
  cabang = await Cabang.findAll({
    attributes: [
      "id_cabang",
      "nama_cabang",
      "alamat",
      "kontak",
      "is_aktif",
      "longitude",
      "latitude",
    ],
    where: {
      id_cabang: id_cabang,
    },
    order: [["id_cabang", "asc"]],
  });

  return cabang;
};

module.exports = {
  getCabang,
  getCabangById,
  Cabang,
};
