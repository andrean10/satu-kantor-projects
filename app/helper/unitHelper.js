const { Op } = require("sequelize");
const { Unit, Cabang } = require("../model/index");

const getUnitId = async (unit_id) => {
  let unit = "";
  if (unit_id[0] != "" && unit_id != null) {
    unit = await Unit.findAll({
      attributes: ["id_unit", "nama_unit"],
      order: [["id_unit", "asc"]],
      where: {
        id_unit: {
          [Op.in]: unit_id,
        },
      },
    });
  }

  return unit;
};

const getUnitAll = async (id_client, id_cabang) => {
  let unit = await Unit.findAll({
    where: {
      id_client: id_client,
      id_cabang: id_cabang,
    },
    order: [["id_unit", "asc"]],
  });
  return unit;
};

const getUnit = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.nama_cabang != "" && filter.nama_cabang != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$cabang.nama_cabang$": {
        [Op.iLike]: "%" + filter.nama_cabang + "%",
      },
    };
  }
  if (filter.nama_unit != "" && filter.nama_unit != null) {
    whereObj.filter = {
      ...whereObj.filter,
      nama_unit: {
        [Op.iLike]: "%" + filter.nama_unit + "%",
      },
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let unit = "";
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        nama_unit: {
          [Op.iLike]: "%" + search + "%",
        },
        "$cabang.nama_cabang$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Cabang,
    },
  ];
  unit = await Unit.findAll({
    order: [["id_unit", "asc"]],
    where: pushWhere,
    limit: limit,
    offset: offset,
    include: getInclude,
  });

  if (
    limit == null &&
    offset == null &&
    search == null &&
    whereObj.filter != null
  ) {
    pushWhere.push(whereObj.filter);
    unit = await Unit.findAll({
      order: [["id_unit", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }
  return unit;
};

const getUnitById = async (unit_id) => {
  let unit = "";
  let getInclude = [
    {
      model: Cabang,
    },
  ];
  if (unit_id != "") {
    unit = await Unit.findOne({
      where: {
        id_unit: unit_id,
      },
      order: [["id_unit", "asc"]],
      include: getInclude,
    });
  }

  return unit;
};
const unitHasManyPegawai = async (unit_id = []) => {
  return unit_id;
};
module.exports = {
  getUnitId,
  getUnitAll,
  getUnit,
  Unit,
  unitHasManyPegawai,
  getUnitById,
};
