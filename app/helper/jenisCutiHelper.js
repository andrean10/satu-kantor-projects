const { Op } = require("sequelize");
const { JenisCuti } = require("../model/index");

const getJenisCuti = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.jenis_cuti != "" && filter.jenis_cuti != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_cuti: {
        [Op.iLike]: "%" + filter.jenis_cuti + "%",
      },
    };
  }

  if (filter.saldo != "" && filter.saldo != null) {
    whereObj.filter = {
      ...whereObj.filter,
      saldo: filter.saldo,
    };
  }

  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let jenis_cuti = "";
  // condition
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        jenis_cuti: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });
  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  jenis_cuti = await JenisCuti.findAll({
    where: pushWhere,
    order: [["id_jenis_cuti", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    jenis_cuti = await JenisCuti.findAll({
      where: pushWhere,
      order: [["id_jenis_cuti", "asc"]],
    });
  }

  return jenis_cuti;
};

module.exports = {
  getJenisCuti,
  JenisCuti,
};
