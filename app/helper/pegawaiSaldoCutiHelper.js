const { Op } = require("sequelize");
const {
  PegawaiSaldoCuti,
  Pegawai,
  JenisCuti,
  Client,
} = require("../model/index");

const getPegawaiSaldoCuti = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.nama_lengkap != "" && filter.nama_lengkap != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.nama_lengkap + "%",
      },
    };
  }
  if (filter.id_jenis_cuti != "" && filter.id_jenis_cuti != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_jenis_cuti: filter.id_jenis_cuti,
    };
  }
  if (filter.saldo != "" && filter.saldo != null) {
    whereObj.filter = {
      ...whereObj.filter,
      saldo: filter.saldo,
    };
  }
  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai: filter.id_pegawai,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pegawaiSaldoCuti = "";
  if (search != null && search) {
    whereObj.queryLike = {
      [Op.or]: {
        "$jenis_cuti.jenis_cuti$": {
          [Op.iLike]: "%" + search + "%",
        },
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    "$pegawai.client.id_client$": id_client,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: JenisCuti,
    },
    {
      model: Pegawai,
      include: [
        {
          model: Client,
          required: true,
        },
      ],
    },
  ];
  pegawaiSaldoCuti = await PegawaiSaldoCuti.findAll({
    order: [["id_pegawai_saldo_cuti", "asc"]],
    where: pushWhere,
    include: getInclude,
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pegawaiSaldoCuti = await PegawaiSaldoCuti.findAll({
      order: [["id_pegawai_saldo_cuti", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return pegawaiSaldoCuti;
};

const getPegawaiSaldoCutiAll = async () => {
  let pegawaiSaldoCuti = await PegawaiSaldoCuti.findAll();
  return pegawaiSaldoCuti;
};

const getPegawaiSaldoCutiId = async (id_pegawai_saldo_cuti = null) => {
  let pegawaiSaldoCuti = null;
  let getInclude = [
    {
      model: JenisCuti,
    },
    {
      model: Pegawai,
    },
  ];
  if (id_pegawai_saldo_cuti != null) {
    pegawaiSaldoCuti = await PegawaiSaldoCuti.findOne({
      where: {
        id_pegawai_saldo_cuti: id_pegawai_saldo_cuti,
      },
      include: getInclude,
    });
  }
  return pegawaiSaldoCuti;
};

const createPegawaiSaldoCuti = async (data = {}, transaction = {}) => {
  let insert = await PegawaiSaldoCuti.create(data, transaction);
  return insert;
};

const updatePegawaiSaldoCuti = async (
  data = {},
  id_pegawai_saldo_cuti,
  transaction = {}
) => {
  let update = await PegawaiSaldoCuti.update(data, {
    where: {
      id_pegawai_saldo_cuti: id_pegawai_saldo_cuti,
    },
    ...transaction,
  });
  return update;
};

const deletePegawaiSaldoCuti = async (
  id_pegawai_saldo_cuti = null,
  transaction = {}
) => {
  let pegawaiSaldoCuti = await PegawaiSaldoCuti.destroy({
    where: {
      id_pegawai_saldo_cuti: id_pegawai_saldo_cuti,
    },
    ...transaction,
  });
  return pegawaiSaldoCuti;
};

module.exports = {
  getPegawaiSaldoCutiAll,
  getPegawaiSaldoCuti,
  getPegawaiSaldoCutiId,
  createPegawaiSaldoCuti,
  updatePegawaiSaldoCuti,
  deletePegawaiSaldoCuti,
  PegawaiSaldoCuti,
};
