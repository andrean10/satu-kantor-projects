const { Op } = require("sequelize");
const {
  PengajuanCuti,
  Cabang,
  Pegawai,
  JenisCuti,
  Client,
} = require("../model/index");
const moment = require("moment");

const getPengajuanCuti = async (
  limit = null,
  offset = null,
  search = null,
  filter = {},
  id_client = null
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.id_jenis_cuti != "" && filter.id_jenis_cuti != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_jenis_cuti: filter.id_jenis_cuti,
    };
  }
  if (filter.id_pegawai != "" && filter.id_pegawai != null) {
    whereObj.filter = {
      ...whereObj.filter,
      "$pegawai.nama_lengkap$": {
        [Op.iLike]: "%" + filter.id_pegawai + "%",
      },
    };
  }
  if (filter.id_pegawai_id != "" && filter.id_pegawai_id != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai: filter.id_pegawai_id,
    };
  }
  if (filter.tanggal_awal_cuti != "" && filter.tanggal_awal_cuti != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_awal_cuti: moment(filter.tanggal_awal_cuti, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.tanggal_akhir_cuti != "" && filter.tanggal_akhir_cuti != null) {
    whereObj.filter = {
      ...whereObj.filter,
      tanggal_akhir_cuti: moment(
        filter.tanggal_akhir_cuti,
        "DD-MM-YYYY"
      ).format("YYYY-MM-DD"),
    };
  }
  if (
    filter.id_pegawai_pengganti != "" &&
    filter.id_pegawai_pengganti != null
  ) {
    whereObj.filter = {
      ...whereObj.filter,
      id_pegawai_pengganti: filter.id_pegawai_pengganti,
    };
  }
  if (filter.catatan != "" && filter.catatan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      catatan: filter.catatan,
    };
  }
  if (filter.id_atasan != "" && filter.id_atasan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      id_atasan: filter.id_atasan,
    };
  }
  if (filter.is_setuju != "" && filter.is_setuju != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_setuju: filter.is_setuju,
    };
  }
  if (filter.waktu_pengajuan != "" && filter.waktu_pengajuan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_pengajuan: moment(filter.waktu_pengajuan, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }
  if (filter.waktu_setuju != "" && filter.waktu_setuju != null) {
    whereObj.filter = {
      ...whereObj.filter,
      waktu_setuju: moment(filter.waktu_setuju, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let pengajuanCuti = "";

  // execute
  let pushWhere = [];
  if (id_client != null) {
    pushWhere.push({
      "$pegawai.client.id_client$": id_client,
    });
  }
  if (search != null) {
    whereObj.queryLike = {
      [Op.or]: {
        "$pegawai.nama_lengkap$": {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  if (search != null) {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  let getInclude = [
    {
      model: Pegawai,
      include: [
        {
          model: Client,
        },
      ],
    },
    {
      model: JenisCuti,
    },
  ];

  pengajuanCuti = await PengajuanCuti.findAll({
    where: pushWhere,
    include: getInclude,
    order: [["id_pengajuan_cuti", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    pengajuanCuti = await PengajuanCuti.findAll({
      order: [["id_pengajuan_cuti", "asc"]],
      where: pushWhere,
      include: getInclude,
    });
  }

  return pengajuanCuti;
};

const getPengajuanCutiAll = async () => {
  let pegawaiCatatan = await PengajuanCuti.findAll();
  return pegawaiCatatan;
};

const getPengajuanCutiId = async (id_pengajuan_cuti = null) => {
  let pengajuanCuti = null;
  let getInclude = [
    {
      model: Pegawai,
    },
    {
      model: JenisCuti,
    },
  ];
  if (id_pengajuan_cuti != null) {
    pengajuanCuti = await PengajuanCuti.findOne({
      where: {
        id_pengajuan_cuti: id_pengajuan_cuti,
      },
      include: getInclude,
    });
  }
  return pengajuanCuti;
};

const createPengajuanCuti = async (data = {}, transaction = {}) => {
  let insert = await PengajuanCuti.create(data, transaction);
  return insert;
};

const updatePengajuanCuti = async (
  data = {},
  id_pengajuan_cuti,
  transaction = {}
) => {
  let update = await PengajuanCuti.update(data, {
    where: {
      id_pengajuan_cuti: id_pengajuan_cuti,
    },
    ...transaction,
  });
  return update;
};

const deletePengajuanCuti = async (
  id_pengajuan_cuti = null,
  transaction = {}
) => {
  let pegawaiCatatan = await PengajuanCuti.destroy({
    where: {
      id_pengajuan_cuti: id_pengajuan_cuti,
    },
    ...transaction,
  });
  return pegawaiCatatan;
};

module.exports = {
  getPengajuanCutiAll,
  getPengajuanCuti,
  getPengajuanCutiId,
  createPengajuanCuti,
  updatePengajuanCuti,
  deletePengajuanCuti,
  PengajuanCuti,
};
