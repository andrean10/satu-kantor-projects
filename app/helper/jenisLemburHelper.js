const { Op } = require("sequelize");
const { JenisLembur } = require("../model/index");

const getJenisLembur = async (
  limit = null,
  offset = null,
  search = null,
  client_id = null,
  filter = {}
) => {
  let whereObj = {};
  whereObj.queryLike = null;
  whereObj.filter = null;

  // filter
  if (filter.jenis_lembur != "" && filter.jenis_lembur != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_lembur: {
        [Op.iLike]: "%" + filter.jenis_lembur + "%",
      },
    };
  }
  if (filter.jenis_jadwal != "" && filter.jenis_jadwal != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_jadwal: {
        [Op.iLike]: "%" + filter.jenis_jadwal + "%",
      },
    };
  }

  if (filter.jenis_perhitungan != "" && filter.jenis_perhitungan != null) {
    whereObj.filter = {
      ...whereObj.filter,
      jenis_perhitungan: {
        [Op.iLike]: "%" + filter.jenis_perhitungan + "%",
      },
    };
  }

  if (filter.is_aktif != "" && filter.is_aktif != null) {
    whereObj.filter = {
      ...whereObj.filter,
      is_aktif: filter.is_aktif,
    };
  }

  if (whereObj.filter != null) {
    whereObj.filter = {
      [Op.and]: whereObj.filter,
    };
  }

  let jenis_lembur = "";
  if (search != null && search != "") {
    whereObj.queryLike = {
      [Op.or]: {
        jenis_lembur: {
          [Op.iLike]: "%" + search + "%",
        },
        jenis_jadwal: {
          [Op.iLike]: "%" + search + "%",
        },
        jenis_perhitungan: {
          [Op.iLike]: "%" + search + "%",
        },
        formula: {
          [Op.iLike]: "%" + search + "%",
        },
      },
    };
  }

  // execute
  let pushWhere = [];
  pushWhere.push({
    id_client: client_id,
  });

  if (search != null && search != "") {
    pushWhere.push(whereObj.queryLike);
  }

  if (whereObj.filter != null) {
    pushWhere.push(whereObj.filter);
  }

  jenis_lembur = await JenisLembur.findAll({
    where: pushWhere,
    order: [["id_jenis_lembur", "asc"]],
    limit: limit,
    offset: offset,
  });

  if (limit == null && offset == null) {
    jenis_lembur = await JenisLembur.findAll({
      where: pushWhere,
      order: [["id_jenis_lembur", "asc"]],
    });
  }
  return jenis_lembur;
};

const getJenisLemburByClientId = async (client_id = null) => {
  let jenis_lembur = "";
  jenis_lembur = await JenisLembur.findAll({
    attributes: [
      "id_jenis_lembur",
      "id_client",
      "jenis_lembur",
      "is_aktif",
      "jenis_jadwal",
      "jenis_perhitungan",
      "formula",
    ],
    where: {
      id_client: client_id,
    },

    order: [["id_jenis_lembur", "asc"]],
  });
  return jenis_lembur;
};

module.exports = {
  JenisLembur,
  getJenisLembur,
  getJenisLemburByClientId,
};
