const { Op } = require("sequelize");
const sequelize = require("../config/db");
const { Izin } = require("../model/index");

const getIzin = async (pegawai_id = null) => {
  let rekening = "";
  if (pegawai_id != null) {
    rekening = await Izin.findAll({
      where: {
        pegawai_id: pegawai_id,
      },
      order: [["pegawai_id", "asc"]],
    });
  } else {
    rekening = await Izin.findAll({
      order: [["pegawai_id", "asc"]],
    });
  }

  return rekening;
};

const insertIzin = async (data = [], transaction = {}) => {
  let rekening = "";
  rekening = Izin.create(data, transaction);
  return rekening;
};

const updateIzin = async (data = [], pegawai_id = null, transaction = {}) => {
  let rekening = "";
  rekening = Izin.update(data, {
    where: {
      pegawai_id: pegawai_id,
    },
    ...transaction,
  });
  return rekening;
};

module.exports = {
  getIzin,
  insertIzin,
  updateIzin,
  Izin,
};
