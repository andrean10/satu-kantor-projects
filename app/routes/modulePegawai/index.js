const express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const formidable = require("formidable");

const pegawaiJadwalController = require("../../controller/ModulePegawai/PegawaiJadwalController");
const pegawaiDashboardController = require("../../controller/ModulePegawai/PegawaiDashboardController");
const jabatanPegawaiController = require("../../controller/ModulePegawai/JabatanPegawaiController");
const unitPegawaiController = require("../../controller/ModulePegawai/UnitPegawaiController");
const catatanPegawaiController = require("../../controller/ModulePegawai/CatatanPegawaiController");
const pegawaiKontrakController = require("../../controller/ModulePegawai/PegawaiKontrakController");
const saldoCutiController = require("../../controller/ModulePegawai/PegawaiSaldoCutiController");
const pegawaiClientController = require("../../controller/ModulePegawai/PegawaiClientController");
const pengajuanGajiController = require("../../controller/ModulePegawai/PengajuanGajiController");
const pengajuanCutiController = require("../../controller/ModulePegawai/PengajuanCutiController");
const pengajuanLemburController = require("../../controller/ModulePegawai/PengajuanLemburController");
const penugasanController = require("../../controller/ModuleMaster/PenugasanController");

const formDataImport = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.importData = files;
    if (err) {
      next(err);
    }
    next();
  });
};

// pegawaiClient
router.route("/pegawaiClient").get(pegawaiClientController.index);

// pegawaiDashboard
router.route("/pegawaiDashboard").get(pegawaiDashboardController.index);

// pegawaiJadwal
router
  .route("/pegawaiJadwal")
  .get(pegawaiJadwalController.index)
  .post(
    body("id_pegawai").notEmpty().withMessage("Pegawai wajib diisi").trim(),
    body("id_jadwal").notEmpty().withMessage("Jadwal wajib diisi").trim(),
    body("bulan").custom(async (value, meta) => {
      const { page } = meta.req.body;
      if (page == "add") {
        let data = value;
        data = value.split(",");
        if (data[0] == "") {
          return Promise.reject("Bulan wajib diisi");
        }
      }
    }),
    body("hari_libur").custom(async (value, meta) => {
      const { page } = meta.req.body;
      if (page == "add") {
        let data = value;
        data = value.split(",");
        if (data[0] == "") {
          return Promise.reject("Hari libur wajib diisi");
        }
      }
    }),
    pegawaiJadwalController.store
  );

router.route("/pegawaiJadwal/submitPerUnit").post(
  body("id_unit").notEmpty().withMessage("Unit wajib diisi").trim(),
  body("id_jadwal").notEmpty().withMessage("Jadwal wajib diisi").trim(),
  body("bulan").custom(async (value, meta) => {
    let data = value;
    data = value.split(",");
    if (data[0] == "") {
      return Promise.reject("Bulan wajib diisi");
    }
  }),
  body("hari_libur").custom(async (value, meta) => {
    let data = value;
    data = value.split(",");
    if (data[0] == "") {
      return Promise.reject("Hari libur wajib diisi");
    }
  }),
  pegawaiJadwalController.submitPerUnit
);
router
  .route("/pegawaiJadwal/:id_pegawai_jadwal/edit")
  .get(pegawaiJadwalController.edit);
router
  .route("/pegawaiJadwal/:id_pegawai_jadwal/delete")
  .get(pegawaiJadwalController.deleteData);
router
  .route("/pegawaiJadwal/pegawaiLoadData")
  .get(pegawaiJadwalController.pegawaiLoadData);
router
  .route("/pegawaiJadwal/changeJadwal")
  .post(pegawaiJadwalController.changeJadwal);
router
  .route("/pegawaiJadwal/batalGantiShift")
  .post(pegawaiJadwalController.batalGantiShift);
router.route("/pegawaiJadwal/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  pegawaiJadwalController.importData
);

// jabatanPegawai
router.route("/jabatanPegawai").get(jabatanPegawaiController.index);

// pegawaiUnit
router.route("/unitPegawai").get(unitPegawaiController.index);

// saldoCuti
router
  .route("/saldoCuti")
  .get(saldoCutiController.index)
  .post(
    body("id_jenis_cuti")
      .notEmpty()
      .withMessage("Jenis cuti wajib diisi")
      .trim(),
    body("id_pegawai").notEmpty().withMessage("Pegawai wajib diisi").trim(),
    body("periode_awal")
      .notEmpty()
      .withMessage("Periode awal wajib diisi")
      .trim(),
    body("periode_akhir")
      .notEmpty()
      .withMessage("Periode akhir wajib diisi")
      .trim(),
    saldoCutiController.store
  );
router
  .route("/saldoCuti/:id_pegawai_saldo_cuti/edit")
  .get(saldoCutiController.edit);
router
  .route("/saldoCuti/:id_pegawai_saldo_cuti/delete")
  .get(saldoCutiController.deleteData);
router.route("/saldoCuti/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  saldoCutiController.importData
);

// kontrakPegawai
router
  .route("/kontrakPegawai")
  .get(pegawaiKontrakController.index)
  .post(
    body("tanggal_masuk")
      .notEmpty()
      .withMessage("Tanggal masuk tidak boleh kosong"),
    body("id_pegawai").notEmpty().withMessage("Pegawai wajib diisi"),
    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi"),
    body("id_unit").notEmpty().withMessage("Unit wajib diisi"),
    body("id_jabatan").notEmpty().withMessage("Jabatan wajib diisi"),
    body("jenis_kontrak").notEmpty().withMessage("Jenis kontrak wajib diisi"),
    body("tanggal_mulai").notEmpty().withMessage("Tanggal mulai wajib diisi"),
    // body("tanggal_selesai")
    //   .notEmpty()
    //   .withMessage("Tanggal selesai wajib diisi"),
    body("id_komponen_gaji").custom(async (value, meta) => {
      const { nominal } = meta.req.body;
      let id_komponen_gaji = value;
      // if (id_komponen_gaji == null) {
      //   return Promise.reject("Komponen gaji wajib diisi");
      // }
      let nominal_gaji = nominal;

      id_komponen_gaji = id_komponen_gaji.split(",");
      let error_id = false;
      if (id_komponen_gaji == "" || id_komponen_gaji == null) {
        error_id = true;
      }
      // if (error_id) {
      //   return Promise.reject("Komponen gaji wajib diisi");
      // }

      nominal_gaji = nominal_gaji.split(",");
      let error = false;

      nominal_gaji.map((v, i) => {
        if ((v == "" || v == 0) && id_komponen_gaji[0] != "") {
          error = true;
        }
      });
      if (error) {
        return Promise.reject("Nominal gaji yang dipilih wajib diisi");
      }
    }),
    pegawaiKontrakController.store
  );

router
  .route("/kontrakPegawai/:id_pegawai_kontrak/edit")
  .get(pegawaiKontrakController.edit);
router
  .route("/kontrakPegawai/:id_pegawai_kontrak/detail")
  .get(pegawaiKontrakController.detail);
router
  .route("/kontrakPegawai/:id_unit/unitPegawai")
  .get(pegawaiKontrakController.unitPegawai);
router
  .route("/kontrakPegawai/:id_pegawai_kontrak/delete")
  .get(pegawaiKontrakController.deleteData);
router.route("/kontrakPegawai/import").post(
  formDataImport,
  body("importData").custom(async (value, meta) => {
    const fileImport = meta.req.body.importData;
    const { importData } = fileImport;

    if (importData.size > 0) {
      const imageType = importData.type;
      const mimeType = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ];
      const checkType = mimeType.includes(imageType);
      if (!checkType) {
        return Promise.reject(
          "Format file tidak didukung, format yang didukung yaitu: " +
            mimeType.join(",")
        );
      }
    } else {
      return Promise.reject("File wajib di upload");
    }
  }),
  pegawaiKontrakController.importData
);
// catatanPegawai
router
  .route("/catatanPegawai")
  .get(catatanPegawaiController.index)
  .post(
    body("jenis").notEmpty().withMessage("Jenis wajib diisi").trim(),
    body("id_pegawai").notEmpty().withMessage("Pegawai wajib diisi").trim(),
    body("catatan").notEmpty().withMessage("Catatan wajib diisi").trim(),
    body("waktu_efektif")
      .notEmpty()
      .withMessage("Waktu efektif wajib diisi")
      .trim(),
    catatanPegawaiController.store
  );
router
  .route("/catatanPegawai/:id_pegawai_catatan/edit")
  .get(catatanPegawaiController.edit);
router
  .route("/catatanPegawai/:id_pegawai_catatan/delete")
  .get(catatanPegawaiController.deleteData);

// pengajuanGaji
router
  .route("/pengajuanGaji")
  .get(pengajuanGajiController.index)
  .post(
    body("periode_pengajuan")
      .notEmpty()
      .withMessage("Periode pengajuan diisi")
      .trim(),
    body("periode_awal").notEmpty().withMessage("Periode awal diisi").trim(),
    body("periode_akhir").notEmpty().withMessage("Periode akhir diisi").trim(),
    pengajuanGajiController.store
  );
router
  .route("/pengajuanGaji/:id_pengajuan_gaji/edit")
  .get(pengajuanGajiController.edit);
router
  .route("/pengajuanGaji/:id_pengajuan_gaji/delete")
  .get(pengajuanGajiController.deleteData);

// pengajuanCuti
const formData = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.berkas = files;
    if (err) {
      next(err);
    }
    next();
  });
};

// penugasan
const formNamaFile = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.nama_file = files;

    if (err) {
      next(err);
    }
    next();
  });
};

router
  .route("/pengajuanCuti")
  .get(pengajuanCutiController.index)
  .post(
    formData,
    body("id_jenis_cuti")
      .notEmpty()
      .withMessage("Jenis cuti wajib diisi")
      .trim(),
    body("id_pegawai").notEmpty().withMessage("Pegawai wajib diisi").trim(),
    body("tanggal_awal_cuti")
      .notEmpty()
      .withMessage("Tanggal awal wajib diisi")
      .trim(),
    body("berkas").custom(async (value, meta) => {
      const fileBerkas = meta.req.body.berkas;
      const { berkas } = fileBerkas;

      if (berkas.size > 0) {
        const imageType = berkas.type;
        const size = berkas.size / 1000000;
        const mimeType = ["application/pdf"];
        const byte = 5000000 / 1000000;
        const checkType = mimeType.includes(imageType);
        if (!checkType) {
          return Promise.reject(
            "Format file tidak didukung, format yang didukung yaitu: " +
              mimeType.join(",")
          );
        }
        if (size > byte) {
          return Promise.reject("Ukuran berkas lebih dari 5 mb ");
        }
      }
    }),
    pengajuanCutiController.store
  );
router
  .route("/pengajuanCuti/:id_pengajuan_cuti/edit")
  .get(pengajuanCutiController.edit);
router
  .route("/pengajuanCuti/:id_pengajuan_cuti/delete")
  .get(pengajuanCutiController.deleteData);
router
  .route("/pengajuanCuti/checkPegawaiSaldoCuti")
  .get(pengajuanCutiController.checkPegawaiSaldoCuti);
router
  .route("/pengajuanCuti/checkJadwalPegawai")
  .get(pengajuanCutiController.checkJadwalPegawai);

// pengajuanLembur
router
  .route("/pengajuanLembur")
  .get(pengajuanLemburController.index)
  .post(
    formData,
    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi").trim(),
    body("id_jenis_lembur")
      .notEmpty()
      .withMessage("Jenis lembur wajib diisi")
      .trim(),
    body("tanggal_lembur")
      .notEmpty()
      .withMessage("Tanggal lembur wajib diisi")
      .trim(),
    body("waktu_mulai")
      .notEmpty()
      .withMessage("Waktu mulai wajib diisi")
      .trim(),
    body("waktu_selesai")
      .notEmpty()
      .withMessage("Waktu selesai wajib diisi")
      .trim(),
    body("berkas").custom(async (value, meta) => {
      const fileBerkas = meta.req.body.berkas;
      const { berkas } = fileBerkas;
      if (berkas != undefined) {
        if (berkas.size > 0) {
          const imageType = berkas.type;
          const size = berkas.size / 1000000;
          const mimeType = ["application/pdf"];
          const byte = 5000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran berkas lebih dari 5 mb ");
          }
        }
      }
    }),
    body("id_pegawai").custom(async (value, meta) => {
      let id_pegawai = value;
      if (id_pegawai == null) {
        return Promise.reject("Salah satu pegawai wajib diisi");
      }
    }),
    pengajuanLemburController.store
  );
router
  .route("/pengajuanLembur/:id_pengajuan_lembur/edit")
  .get(pengajuanLemburController.edit);
router
  .route("/pengajuanLembur/:id_pengajuan_lembur/delete")
  .get(pengajuanLemburController.deleteData);
router
  .route("/pengajuanLembur/getPegawaiCabang")
  .get(pengajuanLemburController.getPegawaiCabang);

// penugasan
router
  .route("/penugasan")
  .get(penugasanController.index)
  .post(
    formNamaFile,
    body("id_cabang").notEmpty().withMessage("Cabang wajib diisi").trim(),
    body("id_pegawai").notEmpty().withMessage("Pegawai wajib diisi").trim(),
    body("tanggal_penugasan")
      .notEmpty()
      .withMessage("Tanggal penugasan wajib diisi")
      .trim(),
    body("jenis_penugasan")
      .notEmpty()
      .withMessage("Jenis penugasan wajib diisi")
      .trim(),
    body("id_tugas").notEmpty().withMessage("Tugas wajib diisi").trim(),
    body("id_lokasi_tugas")
      .notEmpty()
      .withMessage("Lokasi tugas wajib diisi")
      .trim(),
    body("waktu_mulai")
      .notEmpty()
      .withMessage("Waktu mulai wajib diisi")
      .trim(),
    body("waktu_selesai")
      .notEmpty()
      .withMessage("Waktu selesai wajib diisi")
      .trim(),
    body("status")
      .notEmpty()
      .withMessage("Status list pekerjaan wajib diisi")
      .trim(),
    penugasanController.store
  );
router.route("/penugasan/:id_penugasan/edit").get(penugasanController.edit);
router
  .route("/penugasan/:id_penugasan/delete")
  .get(penugasanController.deleteData);

module.exports = router;
