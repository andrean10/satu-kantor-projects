const express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const { protect } = require("../../middleware");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const formidable = require("formidable");
const moment = require("moment");

// controller
const loginController = require("../../controller/ApiModulePegawai/LoginController");
const profileController = require("../../controller/ApiModulePegawai/ProfileController");
const userController = require("../../controller/ApiModulePegawai/UserController");
const homeController = require("../../controller/ApiModulePegawai/HomeController");
const absenceController = require("../../controller/ApiModulePegawai/AbsenceController");
const profileClientController = require("../../controller/ApiModulePegawai/ProfileClientController");
const pegawaiController = require("../../controller/ApiModulePegawai/PegawaiController");
const pegawaiCutiController = require("../../controller/ApiModulePegawai/PegawaiCutiController");
const pengajuanLemburController = require("../../controller/ApiModulePegawai/PengajuanLemburController");
const pegawaiJadwalController = require("../../controller/ApiModulePegawai/PegawaiJadwalController");
const rainburstmentController = require("../../controller/ApiModulePegawai/RainburstmentController");
const penugasanController = require("../../controller/ApiModulePegawai/PenugasanController");

// helper
const {
  pegawaiHelper,
  userHelper,
  userMappingHelper,
  pegawaiJadwalHelper,
} = require("../../helper/index");
const { json } = require("body-parser");

// api login
router
  .route("/login")
  .post(
    body("email")
      .notEmpty()
      .withMessage("Email wajib diisi")
      .trim()
      .isEmail()
      .withMessage("Email tidak valid"),
    body("password").notEmpty().withMessage("Password wajib diisi").trim(),
    loginController.index
  );
router
  .route("/login/verify")
  .post(
    body("email")
      .notEmpty()
      .withMessage("Email wajib diisi")
      .trim()
      .isEmail()
      .withMessage("Email tidak valid"),
    body("pin").notEmpty().withMessage("Pin wajib diisi").trim(),
    loginController.verify
  );
router
  .route("/login/sendOtpRedo")
  .post(
    body("email")
      .notEmpty()
      .withMessage("Email wajib diisi")
      .trim()
      .isEmail()
      .withMessage("Email tidak valid"),
    loginController.sendOtpRedo
  );

const formData = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.gambar = files;
    if (err) {
      next(err);
    }
    next();
  });
};

// pengajuanCuti
const formDataBerkas = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.berkas = files;
    if (err) {
      next(err);
    }
    next();
  });
};

const formDataStrukPembelian = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.struk_pembelian = files;
    if (err) {
      next(err);
    }
    next();
  });
};

// api profile
router
  .route("/profile")
  .get(protect, profileController.index)
  .put(
    formData,
    protect,
    body("no_identitas")
      .notEmpty()
      .withMessage("No. identitas tidak boleh kosong")
      .trim()
      .isLength({ max: 20 })
      .withMessage("Jenis identitas maksimal 20 karakter"),
    body("no_identitas").custom(async (value, meta) => {
      let user = meta.req.user.data;
      const id_pegawai = user.id_mapping;

      let check = await pegawaiHelper.checkIdentitas(
        "no_identitas",
        "edit",
        value,
        id_pegawai
      );
      if (check > 0) {
        return Promise.reject("No identitas ini sudah digunakan");
      }
    }),
    body("nama_lengkap")
      .notEmpty()
      .withMessage("Nama lengkap tidak boleh kosong")
      .trim(),
    body("jenis_identitas")
      .notEmpty()
      .withMessage("Jenis identitas tidak boleh kosong")
      .isLength({ max: 10 })
      .withMessage("Jenis identitas maksimal 10 karakter"),
    body("jenis_kelamin")
      .notEmpty()
      .withMessage("Jenis kelamin tidak boleh kosong"),
    body("tempat_lahir")
      .notEmpty()
      .withMessage("Tempat lahir tidak boleh kosong"),
    body("tanggal_lahir")
      .notEmpty()
      .withMessage("Tanggal lahir tidak boleh kosong"),
    body("status_perkawinan")
      .notEmpty()
      .withMessage("Status perkawinan tidak boleh kosong"),
    body("agama").notEmpty().withMessage("Agama tidak boleh kosong"),
    body("pendidikan").notEmpty().withMessage("Pendidikan tidak boleh kosong"),
    body("alamat_domisili")
      .notEmpty()
      .withMessage("Alamat domisili tidak boleh kosong"),
    body("alamat_ktp").notEmpty().withMessage("Alamat KTP tidak boleh kosong"),
    body("no_kontak1")
      .notEmpty()
      .withMessage("No. kontak tidak boleh kosong")
      .isInt()
      .withMessage("No. kontak wajib angka")
      .isLength({ max: 15 })
      .withMessage("Jenis identitas maksimal 15 karakter"),
    body("email")
      .notEmpty()
      .withMessage("Email tidak boleh kosong")
      .isEmail()
      .withMessage("Email tidak valid"),
    body("no_pegawai")
      .notEmpty()
      .withMessage("No. pegawai tidak boleh kosong")
      .isInt()
      .withMessage("No pegawai wajib angka")
      .isLength({ max: 25 })
      .withMessage("Jenis identitas maksimal 25 karakter"),
    body("no_pegawai").custom(async (value, meta) => {
      let user = meta.req.user.data;
      const id_pegawai = user.id_mapping;
      let check = await pegawaiHelper.checkIdentitas(
        "no_pegawai",
        "edit",
        value,
        id_pegawai
      );
      if (check > 0) {
        return Promise.reject("No. pegawai ini sudah digunakan");
      }
    }),
    body("gambar").custom(async (value, meta) => {
      const fileGambar = meta.req.body.gambar;
      const { gambar } = fileGambar;
      if (gambar != undefined) {
        if (gambar.size > 0) {
          const imageType = gambar.type;
          const size = gambar.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
          ];
          const byte = 14000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran gambar lebih dari 14 mb ");
          }
        }
      }
    }),
    profileController.store
  );

router.route("/profile/gantiPhoto").put(
  formData,
  protect,
  body("gambar").custom(async (value, meta) => {
    const fileGambar = meta.req.body.gambar;
    const { gambar } = fileGambar;
    if (gambar != undefined) {
      if (gambar.size > 0) {
        const imageType = gambar.type;
        const size = gambar.size / 1000000;
        const mimeType = [
          "image/jpg",
          "image/jpeg",
          "image/gif",
          "image/png",
          "image/svg+xml",
        ];
        const byte = 14000000 / 1000000;
        const checkType = mimeType.includes(imageType);
        if (!checkType) {
          return Promise.reject(
            "Format file tidak didukung, format yang didukung yaitu: " +
              mimeType.join(",")
          );
        }
        if (size > byte) {
          return Promise.reject("Ukuran gambar lebih dari 14 mb ");
        }
      }
    } else {
      return Promise.reject("Gambar wajib diupload");
    }
  }),
  profileController.changePhotoProfile
);

// api profileClient
router
  .route("/profileClient")
  .get(protect, profileClientController.store)
  .put(
    protect,
    body("nama_client")
      .notEmpty()
      .withMessage("Nama client tidak boleh kosong")
      .trim(),
    body("alamat").notEmpty().withMessage("Alamat tidak boleh kosong").trim(),
    body("kontak").notEmpty().withMessage("Kontak tidak boleh kosong").trim(),
    profileClientController.store
  );

// api user
router
  .route("/user")
  .get(protect, userController.index)
  .put(
    protect,
    body("username").custom(async (value, meta) => {
      let user = meta.req.user.data;
      const id_user = user.id;
      let check = await userHelper.checkIdentitas(
        "username",
        "edit",
        value,
        id_user
      );
      if (check > 0) {
        return Promise.reject("Username ini sudah digunakan");
      }
    }),
    body("password_hash").custom(async (value, meta) => {
      const { confirm_password_hash } = meta.req.body;
      if (value != null && confirm_password_hash != null) {
        if (value != confirm_password_hash) {
          return Promise.reject("Password tidak sama dengan confirm password");
        }
      }
    }),
    body("password_hash_old").custom(async (value, meta) => {
      let data = meta.req.user.data;
      // get data maping
      const getMapping = await userMappingHelper.getJoinDataMapping(
        data.jenis_mapping,
        null,
        data.id_user_mapping
      );

      const password_hash_old = getMapping.user.password_hash;
      let hash = password_hash_old;
      let check = bcrypt.compareSync(value, hash);
      if (check == false) {
        return Promise.reject("Password lama tidak sama dengan sebelumnya");
      }
    }),
    body("email")
      .notEmpty()
      .withMessage("Email wajib diisi")
      .trim()
      .isEmail()
      .withMessage("Email tidak valid"),
    userController.store
  );

// api home
router.route("/home").get(protect, homeController.index);

// api absensce
router.route("/checkIn").post(
  protect,
  body("radius").custom(async (value, meta) => {
    let body = meta.req.body;
    let user = meta.req.user.data;

    const { radius } = body;

    const getMapping = await userMappingHelper.getJoinDataMapping(
      user.jenis_mapping,
      null,
      user.id_user_mapping
    );
    const tanggal_sekarang = moment().format("YYYY-MM-DD");
    let jadwal = await pegawaiJadwalHelper.getPegawaiByIdJadwal(
      getMapping.pegawai.id_pegawai,
      tanggal_sekarang
    );

    // waktu toleransi
    let getJadwal = jadwal.jadwal;
    let jadwalWaktuMasuk =
      moment().format("YYYY-MM-DD") + " " + getJadwal.waktu_masuk;
    let minSepuluhWaktuMasuk = moment(jadwalWaktuMasuk);
    minSepuluhWaktuMasuk = minSepuluhWaktuMasuk.subtract(10, "minutes");
    let waktuToleransi = moment(minSepuluhWaktuMasuk).format(
      "YYYY-MM-DD HH:mm:ss"
    );

    // waktu sekarang
    let waktuSekarang = moment().format("YYYY-MM-DD HH:mm:ss");
    let duration =
      moment.duration(moment(waktuToleransi)).asSeconds() -
      moment.duration(moment(waktuSekarang)).asSeconds();

    let getToleransi = moment.duration(getJadwal.waktu_masuk).asSeconds();
    let sepuluhMenit = 60 * 10;
    let durasiWaktu = getToleransi - sepuluhMenit;
    durasiWaktu = moment
      .utc(moment.duration(durasiWaktu, "seconds").asMilliseconds())
      .format("HH:mm");

    if (duration > 0) {
      return Promise.reject("Anda hanya boleh check in jam " + durasiWaktu);
    }

    if (radius == null || radius == false) {
      return Promise.reject("Anda tidak berada pada area kantor");
    }
  }),
  absenceController.index
);
router.route("/checkOut").post(
  protect,
  body("radius").custom(async (value, meta) => {
    let body = meta.req.body;

    const { radius } = body;

    if (radius == null || radius == false) {
      return Promise.reject("Anda tidak berada pada area kantor");
    }
  }),
  absenceController.checkOut
);
router.route("/history").get(protect, absenceController.history);
router
  .route("/alasanTerlambat")
  .post(protect, absenceController.alasanTerlambat);
router
  .route("/alasanPulangCepat")
  .post(protect, absenceController.alasanPulangCepat);
router.route("/jadwal").get(protect, absenceController.jadwal);

// api pegawai
router
  .route("/client/pegawaiHistory")
  .get(protect, pegawaiController.pegawaiHistory);

router.route("/client/pegawai").get(protect, pegawaiController.index);

// api pegawai jabatan
router
  .route("/pegawai/membawahiUnit")
  .get(protect, pegawaiController.membawahiUnit);
router
  .route("/pegawai/membawahiJabatan")
  .get(protect, pegawaiController.membawahiJabatan);

module.exports = router;

// pegawai cuti
router
  .route("/pegawaiCuti")
  .get(protect, pegawaiCutiController.index)
  .post(
    formDataBerkas,
    protect,
    body("id_jenis_cuti").notEmpty().withMessage("Jenis cuti wajib diisi"),
    body("tanggal_awal_cuti")
      .notEmpty()
      .withMessage("Tanggal awal cuti wajib diisi"),
    body("tanggal_akhir_cuti")
      .notEmpty()
      .withMessage("Tanggal akhir cuti wajib diisi"),
    body("berkas").custom(async (value, meta) => {
      const fileGambar = meta.req.body.berkas;
      const { berkas } = fileGambar;

      if (berkas != undefined) {
        if (berkas.size > 0) {
          const imageType = berkas.type;
          const size = berkas.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
            "application/pdf",
          ];
          const byte = 14000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran berkas lebih dari 14 mb ");
          }
        }
      }
    }),
    pegawaiCutiController.store
  );
router
  .route("/pegawaiCuti/:id_pengajuan_cuti")
  .get(protect, pegawaiCutiController.edit)
  .put(
    formDataBerkas,
    protect,
    body("id_jenis_cuti").notEmpty().withMessage("Jenis cuti wajib diisi"),
    body("tanggal_awal_cuti")
      .notEmpty()
      .withMessage("Tanggal awal cuti wajib diisi"),
    body("tanggal_akhir_cuti")
      .notEmpty()
      .withMessage("Tanggal akhir cuti wajib diisi"),
    body("berkas").custom(async (value, meta) => {
      const fileGambar = meta.req.body.berkas;
      const { berkas } = fileGambar;

      if (berkas != undefined) {
        if (berkas.size > 0) {
          const imageType = berkas.type;
          const size = berkas.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
            "application/pdf",
          ];
          const byte = 14000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran berkas lebih dari 14 mb ");
          }
        }
      }
    }),
    pegawaiCutiController.update
  )
  .delete(protect, pegawaiCutiController.deleteData);

router
  .route("/pegawaiCuti/saldoCuti/check")
  .get(protect, pegawaiCutiController.saldoCuti);

// pengajuan lembur
router
  .route("/pengajuanLembur")
  .get(protect, pengajuanLemburController.index)
  .post(
    formDataBerkas,
    protect,
    body("tanggal_lembur")
      .notEmpty()
      .withMessage("Tanggal lembur wajib diisi")
      .trim(),
    body("waktu_mulai")
      .notEmpty()
      .withMessage("Waktu mulai wajib diisi")
      .trim(),
    body("waktu_selesai")
      .notEmpty()
      .withMessage("Waktu selesai wajib diisi")
      .trim(),
    body("id_jenis_lembur")
      .notEmpty()
      .withMessage("Jenis lembur wajib diisi")
      .trim(),
    pengajuanLemburController.store
  );
router
  .route("/pengajuanLembur/:id_pengajuan_lembur")
  .get(protect, pengajuanLemburController.edit)
  .put(
    formDataBerkas,
    protect,
    body("tanggal_lembur")
      .notEmpty()
      .withMessage("Tanggal lembur wajib diisi")
      .trim(),
    body("waktu_mulai")
      .notEmpty()
      .withMessage("Waktu mulai wajib diisi")
      .trim(),
    body("waktu_selesai")
      .notEmpty()
      .withMessage("Waktu selesai wajib diisi")
      .trim(),
    body("id_jenis_lembur")
      .notEmpty()
      .withMessage("Jenis lembur wajib diisi")
      .trim(),
    pengajuanLemburController.update
  )
  .delete(protect, pengajuanLemburController.deleteData);
router
  .route("/pengajuanLembur/check/jenisLembur")
  .get(protect, pengajuanLemburController.jenisLembur);

// pegawai jadwal
router.route("/pegawaiJadwal").get(protect, pegawaiJadwalController.index);
router
  .route("/pegawaiJadwal/:id_pegawai_jadwal")
  .get(protect, pegawaiJadwalController.edit)
  .put(
    protect,
    body("keterangan_pegawai").notEmpty().withMessage("Keterangan wajib diisi"),
    pegawaiJadwalController.update
  )
  .delete(protect, pegawaiJadwalController.deleteData);

// rainburstment
router
  .route("/rainburstment")
  .get(protect, rainburstmentController.index)
  .post(
    formDataStrukPembelian,
    protect,
    body("tanggal").notEmpty().withMessage("Tanggal wajib diisi"),
    body("keterangan").notEmpty().withMessage("Keterangan wajib diisi"),
    body("struk_pembelian").custom(async (value, meta) => {
      const fileStruk = meta.req.body.struk_pembelian;
      const { struk_pembelian } = fileStruk;
      if (struk_pembelian != undefined) {
        if (struk_pembelian.size > 0) {
          const imageType = struk_pembelian.type;
          const size = struk_pembelian.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
            "application/pdf",
          ];
          const byte = 5000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran struk_pembelian lebih dari 5 mb");
          }
        } else {
          return Promise.reject("Struk pembelian wajib diupload");
        }
      } else {
        return Promise.reject("Struk pembelian wajib diupload");
      }
    }),
    body("nama_pembelian").notEmpty().withMessage("Nama pembelian wajib diisi"),
    body("harga").notEmpty().withMessage("Harga wajib diisi"),
    body("jumlah").notEmpty().withMessage("Jumlah wajib diisi"),
    rainburstmentController.store
  );

router
  .route("/rainburstment/:id_rainburstment")
  .get(protect, rainburstmentController.edit)
  .put(
    formDataStrukPembelian,
    protect,
    body("tanggal").notEmpty().withMessage("Tanggal wajib diisi"),
    body("keterangan").notEmpty().withMessage("Keterangan wajib diisi"),
    body("struk_pembelian").custom(async (value, meta) => {
      const fileStruk = meta.req.body.struk_pembelian;
      const { struk_pembelian } = fileStruk;
      if (struk_pembelian != undefined) {
        if (struk_pembelian.size > 0) {
          const imageType = struk_pembelian.type;
          const size = struk_pembelian.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
            "application/pdf",
          ];
          const byte = 5000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran struk_pembelian lebih dari 5 mb");
          }
        } else {
          return Promise.reject("Struk pembelian wajib diupload");
        }
      } else {
        return Promise.reject("Struk pembelian wajib diupload");
      }
    }),
    body("nama_pembelian").notEmpty().withMessage("Nama pembelian wajib diisi"),
    body("harga").notEmpty().withMessage("Harga wajib diisi"),
    body("jumlah").notEmpty().withMessage("Jumlah wajib diisi"),
    rainburstmentController.update
  )
  .delete(protect, rainburstmentController.deleteData);

// penugasan
const formNamaFile = (req, res, next) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    req.body = fields;
    req.body.nama_file = files;

    if (err) {
      next(err);
    }
    next();
  });
};

// penugasan
router.route("/penugasan").get(protect, penugasanController.index);

router
  .route("/penugasan/:id_penugasan")
  .get(protect, penugasanController.edit)
  .put(
    formNamaFile,
    protect,
    body("status").notEmpty().withMessage("Status wajib diisi"),
    body("nama_file").custom(async (value, meta) => {
      const fileUpload = meta.req.body.nama_file;
      const { nama_file } = fileUpload;
      if (nama_file != undefined) {
        if (nama_file.size > 0) {
          const imageType = nama_file.type;
          const size = nama_file.size / 1000000;
          const mimeType = [
            "image/jpg",
            "image/jpeg",
            "image/gif",
            "image/png",
            "image/svg+xml",
            "application/pdf",
          ];
          const byte = 14000000 / 1000000;
          const checkType = mimeType.includes(imageType);
          if (!checkType) {
            return Promise.reject(
              "Format file tidak didukung, format yang didukung yaitu: " +
                mimeType.join(",")
            );
          }
          if (size > byte) {
            return Promise.reject("Ukuran file lebih dari 14 mb");
          }
        } else {
          return Promise.reject("File pengerjaan wajib diupload");
        }
      } else {
        return Promise.reject("File pengerjaan wajib diupload");
      }
    }),
    body("keterangan").notEmpty().withMessage("Keterangan wajib diisi"),
    body("id_penugasan_status")
      .notEmpty()
      .withMessage("Penugasan status wajib diisi"),
    body("id_penugasan_file")
      .notEmpty()
      .withMessage("Penugasan file wajib diisi"),
    penugasanController.update
  );
