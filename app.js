var createError = require("http-errors");
var express = require("express");
const expressLayouts = require("express-ejs-layouts");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const session = require("express-session");
const flash = require("connect-flash");

var userRoute = require("./app/routes/index");
var adminRoute = require("./app/routes/moduleMaster/index");
var pegawaiRoute = require("./app/routes/modulePegawai/index");
var apiRoute = require("./app/routes/apiModulePegawai/index");
var laporanRoute = require("./app/routes/moduleLaporan/index");
const { webProtect, userGuide } = require("./app/middleware");

var app = express();
TZ = "Asia/Jakarta";

// Set Templating Engine
app.use(expressLayouts);
app.set("layout", "./layouts/full-width");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.set("layout extractScripts", true);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use("/node_modules", express.static(path.join(__dirname, "node_modules")));
app.use(
  "/bower_components",
  express.static(path.join(__dirname, "bower_components"))
);

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: "code-satu-kantor",
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
    resave: false,
  })
);

app.use(flash());

app.use("/test", function (req, res) {
  let data = [
    { number: 1, indikantor: "Indikator 1" },
    { number: 1, indikantor: "Indikator 2" },
    { number: 1, indikantor: "Indikator 3" },
    { number: 1, indikantor: "Indikator 4" },
    { number: 2, indikantor: "Indikator 1" },
    { number: 2, indikantor: "Indikator 2" },
    { number: 2, indikantor: "Indikator 3" },
    { number: 2, indikantor: "Indikator 4" },
    { number: 3, indikantor: "Indikator 1" },
    { number: 3, indikantor: "Indikator 2" },
    { number: 3, indikantor: "Indikator 3" },
    { number: 3, indikantor: "Indikator 4" },
  ];
  let getPush = {};
  let check = [];
  data.map((v, i) => {
    if (getPush.hasOwnProperty(v.number)) {
      getPush[v.number] = [...getPush[v.number], v.indikantor];
    } else {
      getPush[v.number] = [v.indikantor];
    }
  });
  const manipulateIndex1 = {
    0: "Fullstack developer",
    1: "Software Enginer",
    2: "Mern Stack Js",
    3: "Javascript developer",
    4: "Mobile app developer",
  };

  Object.keys(getPush).map((v, i) => {
    getPush[v].map((value, index) => {
      if (index == 1) {
        getPush[v][index] = manipulateIndex1;
      }
    });
  });

  return res.status(200).json({
    status: 200,
    message: "ambil data dulu yah ges",
    result: getPush,
  });
});
app.use("/", userGuide, userRoute);
app.use("/admin", webProtect, adminRoute);
app.use("/pegawai", webProtect, pegawaiRoute);
app.use("/laporan", webProtect, laporanRoute);
app.use("/api", apiRoute);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");

  console.log("error message:", err.message);
});

module.exports = app;
